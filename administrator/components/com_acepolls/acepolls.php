<?php
/**
* bilovus.andriy@gmail.com
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once(JPATH_COMPONENT.DS.'controller.php');

// Set the table directory
JTable::addIncludePath(JPATH_COMPONENT.DS.'tables');

// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if (file_exists($path)) {
		require_once $path;
	} else {
		$controller = '';
	}
}
$classname = 'AcepollsController'.ucfirst($controller);

// Create the controller
$controller = new $classname();

$controller->execute(JRequest::getCmd('task'));
$controller->redirect();