<?php
/**
 * @version		$Id: helper.php 963 2012-02-08 17:13:15Z joomlaworks $
 * @package		Frontpage Slideshow
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from JoomlaWorks Ltd.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modeladmin');

class FPSSModelHelper extends JModelAdmin {
	
	protected function loadFormData() {
		$data = $this->getItem();
		return $data;
	}
	
	public function getTable($type = 'category', $prefix = 'FPSS', $config = array()) {
		$type = $this->get('assetType');
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true) {
		JForm::addFormPath(JPATH_COMPONENT.'/models');
		$form = $this->loadForm('com_fpss.'.$this->get('assetType'), $this->get('assetType'), array('control' => '', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}

}
