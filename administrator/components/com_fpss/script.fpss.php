<?php
/**
 * @version		$Id: script.fpss.php 963 2012-02-08 17:13:15Z joomlaworks $
 * @package		Frontpage Slideshow
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from JoomlaWorks Ltd.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

class Com_fpssInstallerScript {
	function update($type) {
		$db = JFactory::getDBO();
		$fields = $db->getTableFields('#__fpss_categories');
		if (!array_key_exists('asset_id', $fields['#__fpss_categories'])) {
			$query = "ALTER TABLE #__fpss_categories ADD `asset_id` int(10) unsigned NOT NULL DEFAULT '0'";
			$db->setQuery($query);
			$db->query();
		}
		$fields = $db->getTableFields('#__fpss_slides');
		if (!array_key_exists('asset_id', $fields['#__fpss_slides'])) {
			$query = "ALTER TABLE #__fpss_slides ADD `asset_id` int(10) unsigned NOT NULL DEFAULT '0'";
			$db->setQuery($query);
			$db->query();
		}
	}
}
