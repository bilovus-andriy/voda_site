<?php
/**
 * @version		$Id: info.php 963 2012-02-08 17:13:15Z joomlaworks $
 * @package		Frontpage Slideshow
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from JoomlaWorks Ltd.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

class FPSSControllerInfo extends JController {

	function display() {
		JRequest::setVar( 'view', 'info' );
		parent::display();
	}

}