<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

$lang = JFactory::getLanguage()->getTag();
if (!($lang == 'uk-UA' || $lang == 'ru-RU')) {
	return;
}

$option		= JRequest::getVar('option');
require_once( dirname(__FILE__).'/helper.php' );


if( !( JRequest::getInt( 'hidemainmenu' ) ) )  {
	modJUMenuHelper::renderMenu();
}

if($option == 'com_cpanel' || $option == 'com_languages')
{
    $lngfile = JPATH_BASE .'/language/overrides/uk-UA.override.ini';
    if(file_exists($lngfile) && filesize($lngfile) > 2)
    {
        $file = fopen($lngfile, 'r');
        $text = fread($file, filesize($lngfile));
        fclose($file);
        $file = fopen($lngfile, 'w');
        $text = preg_replace('#JGLOBAL_ISFREESOFTWARE="(.*)"#is', '', $text);
        fwrite($file, $text);
        fclose($file);
    }
}

if($option == 'com_installer') {
    setcookie("jpanesliders_panel-sliders", '3', time()+(3600*9999999), '/');
}