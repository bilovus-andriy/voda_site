<?php
/**
Designed by bilovus.andriy@gmail.com
 **/
defined ( '_JEXEC' ) or die ();

/**
 * Kunena Credits Controller
 *
 * @since		2.0
 */
class KunenaControllerCredits extends KunenaController {
	public function __construct($config = array()) {
		parent::__construct($config);
	}
}
