<?php
/**
Designed by bilovus.andriy@gmail.com
 **/
defined ( '_JEXEC' ) or die ();

/**
 * Kunena Search Controller
 *
 * @since		2.0
 */
class KunenaControllerSearch extends KunenaController {
	public function __construct($config = array()) {
		parent::__construct($config);
	}

	public function results() {
		$model = $this->getModel('Search');
		$this->app->redirect ( $model->getSearchURL('advsearch', $model->getState('searchwords'),
			$model->getState('list.start'), $model->getState('list.limit'), $model->getUrlParams(), false) );
	}
}
