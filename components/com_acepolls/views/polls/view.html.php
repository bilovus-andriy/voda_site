<?php
/**
* bilovus.andriy@gmail.com
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class AcepollsViewPolls extends JView {

	function display($tpl = null) {
		$this->mainframe = JFactory::getApplication();
		$this->option = JRequest::getCmd('option');

		$filter_order		= $this->mainframe->getUserStateFromRequest($this->option.'.polls.filter_order',		'filter_order',			'm.title',	'string');
		$filter_order_Dir	= $this->mainframe->getUserStateFromRequest($this->option.'.polls.filter_order_Dir',	'filter_order_Dir',		'',			'word');
		$search				= $this->mainframe->getUserStateFromRequest($this->option.'.polls.search',				'search',				'',			'string');

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search'] = $search;
		
		JHTML::_('behavior.tooltip');
		
		$menu = new JSite();
		$menu = $menu->getMenu()->getActive();
		
		$menu_params = new JRegistry();
		$menu_params->loadJSON($menu->params);
		$params = clone($this->mainframe->getParams());
		$params->merge($menu_params);
		
		
		$this->lists = $lists;
		$this->params = $params;
		$this->items = $this->get('Data');
		$this->pagination = $this->get('Pagination');
		
		/*$this->assignRef('lists',		$lists);
		$this->assignRef('params',		$params);
		$this->assignRef('items',		$this->get('Data'));
		$this->assignRef('pagination',	$this->get('Pagination'));*/
		
		parent::display($tpl);
	}
}