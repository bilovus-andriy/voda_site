<?php
/*------------------------------------------------------------------------
mod_bjec - BJ Events Calendar
------------------------------------------------------------------------
author    BestJoom
copyright Copyright (C) 2014 BestJoom.com. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.bestjoom.com
Technical Support:  http://www.bestjoom.com/contact-us/support.html
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
   
  //Global Settings
  $vscript = $params->get('vscript');
  $bjecBannerWidth = 480;
  $bjecBannerHeight = 400;
  
  //EventDay 1 Settings
  $bjec_enable1 = $params->get('bjec_enable1');
  $bjec_day1 = $params->get('bjec_day1');
  $bjec_month1 = $params->get('bjec_month1');
  $bjec_year1 = $params->get('bjec_year1');
  $bjec_color1 =str_replace('#','0x',$params->get('bjec_color1'));
  $bjec_icon1 = $params->get('bjec_icon1');
  $bjec_tooltip1 = $params->get('bjec_tooltip1');
  
  //Event 1 Settings
  $bjecevent_enable1 = $params->get('bjecevent_enable1');
  $bjecevent_image1 = $params->get('bjecevent_image1');
  $bjecevent_url1 = $params->get('bjecevent_url1');
  $bjecevent_button1 = $params->get('bjecevent_button1');
  $bjecevent_title1 = $params->get('bjecevent_title1');
  $bjecevent_description1 = $params->get('bjecevent_description1');
  //Event 2 Settings
  $bjecevent_enable2 = $params->get('bjecevent_enable2');
  $bjecevent_image2 = $params->get('bjecevent_image2');
  $bjecevent_url2 = $params->get('bjecevent_url2');
  $bjecevent_button2 = $params->get('bjecevent_button2');
  $bjecevent_title2 = $params->get('bjecevent_title2');
  $bjecevent_description2 = $params->get('bjecevent_description2');
  
  
  //EventDay 2 Settings
  $bjec_enable2 = $params->get('bjec_enable2');
  $bjec_day2 = $params->get('bjec_day2');
  $bjec_month2 = $params->get('bjec_month2');
  $bjec_year2 = $params->get('bjec_year2');
  $bjec_color2 =str_replace('#','0x',$params->get('bjec_color2'));
  $bjec_icon2 = $params->get('bjec_icon2');
  $bjec_tooltip2 = $params->get('bjec_tooltip2');
  
  //Event 3 Settings
  $bjecevent_enable3 = $params->get('bjecevent_enable3');
  $bjecevent_image3 = $params->get('bjecevent_image3');
  $bjecevent_url3 = $params->get('bjecevent_url3');
  $bjecevent_button3 = $params->get('bjecevent_button3');
  $bjecevent_title3 = $params->get('bjecevent_title3');
  $bjecevent_description3 = $params->get('bjecevent_description3');
  //Event 4 Settings
  $bjecevent_enable4 = $params->get('bjecevent_enable4');
  $bjecevent_image4 = $params->get('bjecevent_image4');
  $bjecevent_url4 = $params->get('bjecevent_url4');
  $bjecevent_button4 = $params->get('bjecevent_button4');
  $bjecevent_title4 = $params->get('bjecevent_title4');
  $bjecevent_description4 = $params->get('bjecevent_description4');
  
  
  //EventDay 3 Settings
  $bjec_enable3 = $params->get('bjec_enable3');
  $bjec_day3 = $params->get('bjec_day3');
  $bjec_month3 = $params->get('bjec_month3');
  $bjec_year3 = $params->get('bjec_year3');
  $bjec_color3 =str_replace('#','0x',$params->get('bjec_color3'));
  $bjec_icon3 = $params->get('bjec_icon3');
  $bjec_tooltip3 = $params->get('bjec_tooltip3');
  
  //Event 5 Settings
  $bjecevent_enable5 = $params->get('bjecevent_enable5');
  $bjecevent_image5 = $params->get('bjecevent_image5');
  $bjecevent_url5 = $params->get('bjecevent_url5');
  $bjecevent_button5 = $params->get('bjecevent_button5');
  $bjecevent_title5 = $params->get('bjecevent_title5');
  $bjecevent_description5 = $params->get('bjecevent_description5');
  //Event 6 Settings
  $bjecevent_enable6 = $params->get('bjecevent_enable6');
  $bjecevent_image6 = $params->get('bjecevent_image6');
  $bjecevent_url6 = $params->get('bjecevent_url6');
  $bjecevent_button6 = $params->get('bjecevent_button6');
  $bjecevent_title6 = $params->get('bjecevent_title6');
  $bjecevent_description6 = $params->get('bjecevent_description6');
  
  
  //EventDay 4 Settings
  $bjec_enable4 = $params->get('bjec_enable4');
  $bjec_day4 = $params->get('bjec_day4');
  $bjec_month4 = $params->get('bjec_month4');
  $bjec_year4 = $params->get('bjec_year4');
  $bjec_color4 =str_replace('#','0x',$params->get('bjec_color4'));
  $bjec_icon4 = $params->get('bjec_icon4');
  $bjec_tooltip4 = $params->get('bjec_tooltip4');
  
  //Event 7 Settings
  $bjecevent_enable7 = $params->get('bjecevent_enable7');
  $bjecevent_image7 = $params->get('bjecevent_image7');
  $bjecevent_url7 = $params->get('bjecevent_url7');
  $bjecevent_button7 = $params->get('bjecevent_button7');
  $bjecevent_title7 = $params->get('bjecevent_title7');
  $bjecevent_description7 = $params->get('bjecevent_description7');
  //Event 8 Settings
  $bjecevent_enable8 = $params->get('bjecevent_enable8');
  $bjecevent_image8 = $params->get('bjecevent_image8');
  $bjecevent_url8 = $params->get('bjecevent_url8');
  $bjecevent_button8 = $params->get('bjecevent_button8');
  $bjecevent_title8 = $params->get('bjecevent_title8');
  $bjecevent_description8 = $params->get('bjecevent_description8');
  
  
  //EventDay 5 Settings
  $bjec_enable5 = $params->get('bjec_enable5');
  $bjec_day5 = $params->get('bjec_day5');
  $bjec_month5 = $params->get('bjec_month5');
  $bjec_year5 = $params->get('bjec_year5');
  $bjec_color5 =str_replace('#','0x',$params->get('bjec_color5'));
  $bjec_icon5 = $params->get('bjec_icon5');
  $bjec_tooltip5 = $params->get('bjec_tooltip5');
  
  //Event 9 Settings
  $bjecevent_enable9 = $params->get('bjecevent_enable9');
  $bjecevent_image9 = $params->get('bjecevent_image9');
  $bjecevent_url9 = $params->get('bjecevent_url9');
  $bjecevent_button9 = $params->get('bjecevent_button9');
  $bjecevent_title9 = $params->get('bjecevent_title9');
  $bjecevent_description9 = $params->get('bjecevent_description9');
  //Event 10 Settings
  $bjecevent_enable10 = $params->get('bjecevent_enable10');
  $bjecevent_image10 = $params->get('bjecevent_image10');
  $bjecevent_url10 = $params->get('bjecevent_url10');
  $bjecevent_button10 = $params->get('bjecevent_button10');
  $bjecevent_title10 = $params->get('bjecevent_title10');
  $bjecevent_description10 = $params->get('bjecevent_description10');
  
  
  //EventDay 6 Settings
  $bjec_enable6 = $params->get('bjec_enable6');
  $bjec_day6 = $params->get('bjec_day6');
  $bjec_month6 = $params->get('bjec_month6');
  $bjec_year6 = $params->get('bjec_year6');
  $bjec_color6 =str_replace('#','0x',$params->get('bjec_color6'));
  $bjec_icon6 = $params->get('bjec_icon6');
  $bjec_tooltip6 = $params->get('bjec_tooltip6');
  
  //Event 11 Settings
  $bjecevent_enable11 = $params->get('bjecevent_enable11');
  $bjecevent_image11 = $params->get('bjecevent_image11');
  $bjecevent_url11 = $params->get('bjecevent_url11');
  $bjecevent_button11 = $params->get('bjecevent_button11');
  $bjecevent_title11 = $params->get('bjecevent_title11');
  $bjecevent_description11 = $params->get('bjecevent_description11');
  //Event 12 Settings
  $bjecevent_enable12 = $params->get('bjecevent_enable12');
  $bjecevent_image12 = $params->get('bjecevent_image12');
  $bjecevent_url12 = $params->get('bjecevent_url12');
  $bjecevent_button12 = $params->get('bjecevent_button12');
  $bjecevent_title12 = $params->get('bjecevent_title12');
  $bjecevent_description12 = $params->get('bjecevent_description12');
  
  
  //EventDay 7 Settings
  $bjec_enable7 = $params->get('bjec_enable7');
  $bjec_day7 = $params->get('bjec_day7');
  $bjec_month7 = $params->get('bjec_month7');
  $bjec_year7 = $params->get('bjec_year7');
  $bjec_color7 =str_replace('#','0x',$params->get('bjec_color7'));
  $bjec_icon7 = $params->get('bjec_icon7');
  $bjec_tooltip7 = $params->get('bjec_tooltip7');
  
  //Event 13 Settings
  $bjecevent_enable13 = $params->get('bjecevent_enable13');
  $bjecevent_image13 = $params->get('bjecevent_image13');
  $bjecevent_url13 = $params->get('bjecevent_url13');
  $bjecevent_button13 = $params->get('bjecevent_button13');
  $bjecevent_title13 = $params->get('bjecevent_title13');
  $bjecevent_description13 = $params->get('bjecevent_description13');
  //Event 14 Settings
  $bjecevent_enable14 = $params->get('bjecevent_enable14');
  $bjecevent_image14 = $params->get('bjecevent_image14');
  $bjecevent_url14 = $params->get('bjecevent_url14');
  $bjecevent_button14 = $params->get('bjecevent_button14');
  $bjecevent_title14 = $params->get('bjecevent_title14');
  $bjecevent_description14 = $params->get('bjecevent_description14');
  
  
  //EventDay 8 Settings
  $bjec_enable8 = $params->get('bjec_enable8');
  $bjec_day8 = $params->get('bjec_day8');
  $bjec_month8 = $params->get('bjec_month8');
  $bjec_year8 = $params->get('bjec_year8');
  $bjec_color8 =str_replace('#','0x',$params->get('bjec_color8'));
  $bjec_icon8 = $params->get('bjec_icon8');
  $bjec_tooltip8 = $params->get('bjec_tooltip8');
  
  //Event 15 Settings
  $bjecevent_enable15 = $params->get('bjecevent_enable15');
  $bjecevent_image15 = $params->get('bjecevent_image15');
  $bjecevent_url15 = $params->get('bjecevent_url15');
  $bjecevent_button15 = $params->get('bjecevent_button15');
  $bjecevent_title15 = $params->get('bjecevent_title15');
  $bjecevent_description15 = $params->get('bjecevent_description15');
  //Event 16 Settings
  $bjecevent_enable16 = $params->get('bjecevent_enable16');
  $bjecevent_image16 = $params->get('bjecevent_image16');
  $bjecevent_url16 = $params->get('bjecevent_url16');
  $bjecevent_button16 = $params->get('bjecevent_button16');
  $bjecevent_title16 = $params->get('bjecevent_title16');
  $bjecevent_description16 = $params->get('bjecevent_description16');
  
  
  //EventDay 9 Settings
  $bjec_enable9 = $params->get('bjec_enable9');
  $bjec_day9 = $params->get('bjec_day9');
  $bjec_month9 = $params->get('bjec_month9');
  $bjec_year9 = $params->get('bjec_year9');
  $bjec_color9 =str_replace('#','0x',$params->get('bjec_color9'));
  $bjec_icon9 = $params->get('bjec_icon9');
  $bjec_tooltip9 = $params->get('bjec_tooltip9');
  
  //Event 17 Settings
  $bjecevent_enable17 = $params->get('bjecevent_enable17');
  $bjecevent_image17 = $params->get('bjecevent_image17');
  $bjecevent_url17 = $params->get('bjecevent_url17');
  $bjecevent_button17 = $params->get('bjecevent_button17');
  $bjecevent_title17 = $params->get('bjecevent_title17');
  $bjecevent_description17 = $params->get('bjecevent_description17');
  //Event 18 Settings
  $bjecevent_enable18 = $params->get('bjecevent_enable18');
  $bjecevent_image18 = $params->get('bjecevent_image18');
  $bjecevent_url18 = $params->get('bjecevent_url18');
  $bjecevent_button18 = $params->get('bjecevent_button18');
  $bjecevent_title18 = $params->get('bjecevent_title18');
  $bjecevent_description18 = $params->get('bjecevent_description18');
  
  
  //EventDay 10 Settings
  $bjec_enable10 = $params->get('bjec_enable10');
  $bjec_day10 = $params->get('bjec_day10');
  $bjec_month10 = $params->get('bjec_month10');
  $bjec_year10 = $params->get('bjec_year10');
  $bjec_color10 =str_replace('#','0x',$params->get('bjec_color10'));
  $bjec_icon10 = $params->get('bjec_icon10');
  $bjec_tooltip10 = $params->get('bjec_tooltip10');
  
  //Event 19 Settings
  $bjecevent_enable19 = $params->get('bjecevent_enable19');
  $bjecevent_image19 = $params->get('bjecevent_image19');
  $bjecevent_url19 = $params->get('bjecevent_url19');
  $bjecevent_button19 = $params->get('bjecevent_button19');
  $bjecevent_title19 = $params->get('bjecevent_title19');
  $bjecevent_description19 = $params->get('bjecevent_description19');
  //Event 20 Settings
  $bjecevent_enable20 = $params->get('bjecevent_enable20');
  $bjecevent_image20 = $params->get('bjecevent_image20');
  $bjecevent_url20 = $params->get('bjecevent_url20');
  $bjecevent_button20 = $params->get('bjecevent_button20');
  $bjecevent_title20 = $params->get('bjecevent_title20');
  $bjecevent_description20 = $params->get('bjecevent_description20');
  
   //head
  global $mainframe;
  $bjecreal = JURI::base();
  $document = &JFactory::getDocument();

  //head start
switch ($vscript) {
    case 'mod1':
        $jsswf_url = $RURL . "modules/mod_bjec/swfobject.js";
        $document->addScript($jsswf_url);
        break;
    case 'mod2':
        $jsswf_url = 'http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js';
        $document->addScript($jsswf_url);
        break;
    case 'mod3':
        $loadswf = '';
        break;
    case 'mod4':
        $compat = 'yes';
        break;
}
  //Create XML
  $xmlfile = JPATH_BASE . "/modules/mod_bjec/xml/data.xml";
  if (is_file($xmlfile)){
   unlink($xmlfile);
  }
  touch($xmlfile) or die("Unable to create: " . $xmlfile);
  $playlist = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
	$playlist .= '<events>';
		
	//EventDay 1 Setting
	if ($bjec_enable1 == '1') {
  		$bjec_enable1 = 'on';
		$playlist .= '<eventday day="' . $bjec_day1 . '" month="' . $bjec_month1 . '" year="' . $bjec_year1 . '" color="' . $bjec_color1 . '" icon="' . $bjec_icon1 . '" tooltip="' . $bjec_tooltip1 . '"';
			
	
	if ($bjecevent_enable1 == '1') {
  		$bjecevent_enable1 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event1.xml"';
	} //if ($bjecevent_enable1 == '2')
  		else {
        $bjecevent_enable1 = 'off';}
		
	if ($bjecevent_enable2 == '1') {
  		$bjecevent_enable2 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event2.xml"';
	} //if ($bjecevent_enable2 == '2')
  		else {
        $bjecevent_enable2 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable1 == '2')
  		else {
        $bjec_enable1 = 'off';}
		
	//EventDay 2 Setting
	if ($bjec_enable2 == '1') {
  		$bjec_enable2 = 'on';
		$playlist .= '<eventday day="' . $bjec_day2 . '" month="' . $bjec_month2 . '" year="' . $bjec_year2 . '" color="' . $bjec_color2 . '" icon="' . $bjec_icon2 . '" tooltip="' . $bjec_tooltip2 . '"';
			
	if ($bjecevent_enable3 == '1') {
  		$bjecevent_enable3 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event3.xml"';
	} //if ($bjecevent_enable3 == '2')
  		else {
        $bjecevent_enable3 = 'off';}
		
	if ($bjecevent_enable4 == '1') {
  		$bjecevent_enable4 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event4.xml"';
	} //if ($bjecevent_enable4 == '2')
  		else {
        $bjecevent_enable4 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable2 == '2')
  		else {
        $bjec_enable2 = 'off';}
		
	//EventDay 3 Setting
	if ($bjec_enable3 == '1') {
  		$bjec_enable3 = 'on';
		$playlist .= '<eventday day="' . $bjec_day3 . '" month="' . $bjec_month3 . '" year="' . $bjec_year3 . '" color="' . $bjec_color3 . '" icon="' . $bjec_icon3 . '" tooltip="' . $bjec_tooltip3 . '"';
			
	if ($bjecevent_enable5 == '1') {
  		$bjecevent_enable5 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event5.xml"';
	} //if ($bjecevent_enable5 == '2')
  		else {
        $bjecevent_enable5 = 'off';}
		
	if ($bjecevent_enable6 == '1') {
  		$bjecevent_enable6 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event6.xml"';
	} //if ($bjecevent_enable6 == '2')
  		else {
        $bjecevent_enable6 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable3 == '2')
  		else {
        $bjec_enable3 = 'off';}
		
	//EventDay 4 Setting
	if ($bjec_enable4 == '1') {
  		$bjec_enable4 = 'on';
		$playlist .= '<eventday day="' . $bjec_day4 . '" month="' . $bjec_month4 . '" year="' . $bjec_year4 . '" color="' . $bjec_color4 . '" icon="' . $bjec_icon4 . '" tooltip="' . $bjec_tooltip4 . '"';
			
	if ($bjecevent_enable7 == '1') {
  		$bjecevent_enable7 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event7.xml"';
	} //if ($bjecevent_enable7 == '2')
  		else {
        $bjecevent_enable7 = 'off';}
		
	if ($bjecevent_enable8 == '1') {
  		$bjecevent_enable8 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event8.xml"';
	} //if ($bjecevent_enable8 == '2')
  		else {
        $bjecevent_enable8 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable4 == '2')
  		else {
        $bjec_enable4 = 'off';}
		
	//EventDay 5 Setting
	if ($bjec_enable5 == '1') {
  		$bjec_enable5 = 'on';
		$playlist .= '<eventday day="' . $bjec_day5 . '" month="' . $bjec_month5 . '" year="' . $bjec_year5 . '" color="' . $bjec_color5 . '" icon="' . $bjec_icon5 . '" tooltip="' . $bjec_tooltip5 . '"';
			
	if ($bjecevent_enable9 == '1') {
  		$bjecevent_enable9 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event9.xml"';
	} //if ($bjecevent_enable9 == '2')
  		else {
        $bjecevent_enable9 = 'off';}
		
	if ($bjecevent_enable10 == '1') {
  		$bjecevent_enable10 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event10.xml"';
	} //if ($bjecevent_enable10 == '2')
  		else {
        $bjecevent_enable10 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable5 == '2')
  		else {
        $bjec_enable5 = 'off';}
		
	//EventDay 6 Setting
	if ($bjec_enable6 == '1') {
  		$bjec_enable6 = 'on';
		$playlist .= '<eventday day="' . $bjec_day6 . '" month="' . $bjec_month6 . '" year="' . $bjec_year6 . '" color="' . $bjec_color6 . '" icon="' . $bjec_icon6 . '" tooltip="' . $bjec_tooltip6 . '"';
			
	if ($bjecevent_enable11 == '1') {
  		$bjecevent_enable11 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event11.xml"';
	} //if ($bjecevent_enable11 == '2')
  		else {
        $bjecevent_enable11 = 'off';}
		
	if ($bjecevent_enable12 == '1') {
  		$bjecevent_enable12 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event12.xml"';
	} //if ($bjecevent_enable12 == '2')
  		else {
        $bjecevent_enable12 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable6 == '2')
  		else {
        $bjec_enable6 = 'off';}
		
	//EventDay 7 Setting
	if ($bjec_enable7 == '1') {
  		$bjec_enable7 = 'on';
		$playlist .= '<eventday day="' . $bjec_day7 . '" month="' . $bjec_month7 . '" year="' . $bjec_year7 . '" color="' . $bjec_color7 . '" icon="' . $bjec_icon7 . '" tooltip="' . $bjec_tooltip7 . '"';
			
	if ($bjecevent_enable13 == '1') {
  		$bjecevent_enable13 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event13.xml"';
	} //if ($bjecevent_enable13 == '2')
  		else {
        $bjecevent_enable13 = 'off';}
		
	if ($bjecevent_enable14 == '1') {
  		$bjecevent_enable14 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event14.xml"';
	} //if ($bjecevent_enable14 == '2')
  		else {
        $bjecevent_enable14 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable7 == '2')
  		else {
        $bjec_enable7 = 'off';}
		
	//EventDay 8 Setting
	if ($bjec_enable8 == '1') {
  		$bjec_enable8 = 'on';
		$playlist .= '<eventday day="' . $bjec_day8 . '" month="' . $bjec_month8 . '" year="' . $bjec_year8 . '" color="' . $bjec_color8 . '" icon="' . $bjec_icon8 . '" tooltip="' . $bjec_tooltip8 . '"';
			
	if ($bjecevent_enable15 == '1') {
  		$bjecevent_enable15 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event15.xml"';
	} //if ($bjecevent_enable15 == '2')
  		else {
        $bjecevent_enable15 = 'off';}
		
	if ($bjecevent_enable16 == '1') {
  		$bjecevent_enable16 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event16.xml"';
	} //if ($bjecevent_enable16 == '2')
  		else {
        $bjecevent_enable16 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable8 == '2')
  		else {
        $bjec_enable8 = 'off';}	
		
	//EventDay 9 Setting
	if ($bjec_enable9 == '1') {
  		$bjec_enable9 = 'on';
		$playlist .= '<eventday day="' . $bjec_day9 . '" month="' . $bjec_month9 . '" year="' . $bjec_year9 . '" color="' . $bjec_color9 . '" icon="' . $bjec_icon9 . '" tooltip="' . $bjec_tooltip9 . '"';
			
	if ($bjecevent_enable17 == '1') {
  		$bjecevent_enable17 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event17.xml"';
	} //if ($bjecevent_enable17 == '2')
  		else {
        $bjecevent_enable17 = 'off';}
		
	if ($bjecevent_enable18 == '1') {
  		$bjecevent_enable18 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event18.xml"';
	} //if ($bjecevent_enable18 == '2')
  		else {
        $bjecevent_enable18 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable9 == '2')
  		else {
        $bjec_enable9 = 'off';}	
		
	//EventDay 10 Setting
	if ($bjec_enable10 == '1') {
  		$bjec_enable10 = 'on';
		$playlist .= '<eventday day="' . $bjec_day10 . '" month="' . $bjec_month10 . '" year="' . $bjec_year10 . '" color="' . $bjec_color10 . '" icon="' . $bjec_icon10 . '" tooltip="' . $bjec_tooltip10 . '"';
			
	if ($bjecevent_enable19 == '1') {
  		$bjecevent_enable19 = 'on';
		$playlist .= 'event1="modules/mod_bjec/xml/event19.xml"';
	} //if ($bjecevent_enable19 == '2')
  		else {
        $bjecevent_enable19 = 'off';}
		
	if ($bjecevent_enable20 == '1') {
  		$bjecevent_enable20 = 'on';
		$playlist .= 'event2="modules/mod_bjec/xml/event20.xml"';
	} //if ($bjecevent_enable20 == '2')
  		else {
        $bjecevent_enable20 = 'off';}
		
		$playlist .= '/>';
		
	} //if ($bjec_enable10 == '2')
  		else {
        $bjec_enable10 = 'off';}		
			
		$playlist .= '</events>';
		
  //Create XML1
  $xml1file = JPATH_BASE . "/modules/mod_bjec/xml/event1.xml";
  if (is_file($xml1file)){
   unlink($xml1file);
  }
  touch($xml1file) or die("Unable to create: " . $xml1file);
  $playlist1 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist1 .= '<event><eventday
		image="' . $bjecevent_image1 . '"
		url="' . $bjecevent_url1 . '"
		button="' . $bjecevent_button1 . '"
		title="' . $bjecevent_title1 . '"
		description="' . $bjecevent_description1 . '
		"/></event>';
		
  //Create XML2
  $xml2file = JPATH_BASE . "/modules/mod_bjec/xml/event2.xml";
  if (is_file($xml2file)){
   unlink($xml2file);
  }
  touch($xml2file) or die("Unable to create: " . $xml2file);
  $playlist2 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist2 .= '<event><eventday
		image="' . $bjecevent_image2 . '"
		url="' . $bjecevent_url2 . '"
		button="' . $bjecevent_button2 . '"
		title="' . $bjecevent_title2 . '"
		description="' . $bjecevent_description2 . '
		"/></event>';
		
  //Create XML3
  $xml3file = JPATH_BASE . "/modules/mod_bjec/xml/event3.xml";
  if (is_file($xml3file)){
   unlink($xml3file);
  }
  touch($xml3file) or die("Unable to create: " . $xml3file);
  $playlist3 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist3 .= '<event><eventday
		image="' . $bjecevent_image3 . '"
		url="' . $bjecevent_url3 . '"
		button="' . $bjecevent_button3 . '"
		title="' . $bjecevent_title3 . '"
		description="' . $bjecevent_description3 . '
		"/></event>';
		
  //Create XML4
  $xml4file = JPATH_BASE . "/modules/mod_bjec/xml/event4.xml";
  if (is_file($xml4file)){
   unlink($xml4file);
  }
  touch($xml4file) or die("Unable to create: " . $xml4file);
  $playlist4 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist4 .= '<event><eventday
		image="' . $bjecevent_image4 . '"
		url="' . $bjecevent_url4 . '"
		button="' . $bjecevent_button4 . '"
		title="' . $bjecevent_title4 . '"
		description="' . $bjecevent_description4 . '
		"/></event>';
		
  //Create XML5
  $xml5file = JPATH_BASE . "/modules/mod_bjec/xml/event5.xml";
  if (is_file($xml5file)){
   unlink($xml5file);
  }
  touch($xml5file) or die("Unable to create: " . $xml5file);
  $playlist5 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist5 .= '<event><eventday
		image="' . $bjecevent_image5 . '"
		url="' . $bjecevent_url5 . '"
		button="' . $bjecevent_button5 . '"
		title="' . $bjecevent_title5 . '"
		description="' . $bjecevent_description5 . '
		"/></event>';
		
  //Create XML6
  $xml6file = JPATH_BASE . "/modules/mod_bjec/xml/event6.xml";
  if (is_file($xml6file)){
   unlink($xml6file);
  }
  touch($xml6file) or die("Unable to create: " . $xml6file);
  $playlist6 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist6 .= '<event><eventday
		image="' . $bjecevent_image6 . '"
		url="' . $bjecevent_url6 . '"
		button="' . $bjecevent_button6 . '"
		title="' . $bjecevent_title6 . '"
		description="' . $bjecevent_description6 . '
		"/></event>';
		
  //Create XML7
  $xml7file = JPATH_BASE . "/modules/mod_bjec/xml/event7.xml";
  if (is_file($xml7file)){
   unlink($xml7file);
  }
  touch($xml7file) or die("Unable to create: " . $xml7file);
  $playlist7 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist7 .= '<event><eventday
		image="' . $bjecevent_image7 . '"
		url="' . $bjecevent_url7 . '"
		button="' . $bjecevent_button7 . '"
		title="' . $bjecevent_title7 . '"
		description="' . $bjecevent_description7 . '
		"/></event>';
		
  //Create XML8
  $xml8file = JPATH_BASE . "/modules/mod_bjec/xml/event8.xml";
  if (is_file($xml8file)){
   unlink($xml8file);
  }
  touch($xml8file) or die("Unable to create: " . $xml8file);
  $playlist8 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist8 .= '<event><eventday
		image="' . $bjecevent_image8 . '"
		url="' . $bjecevent_url8 . '"
		button="' . $bjecevent_button8 . '"
		title="' . $bjecevent_title8 . '"
		description="' . $bjecevent_description8 . '
		"/></event>';
		
  //Create XML9
  $xml9file = JPATH_BASE . "/modules/mod_bjec/xml/event9.xml";
  if (is_file($xml9file)){
   unlink($xml9file);
  }
  touch($xml9file) or die("Unable to create: " . $xml9file);
  $playlist9 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist9 .= '<event><eventday
		image="' . $bjecevent_image9 . '"
		url="' . $bjecevent_url9 . '"
		button="' . $bjecevent_button9 . '"
		title="' . $bjecevent_title9 . '"
		description="' . $bjecevent_description9 . '
		"/></event>';
		
  //Create XML10
  $xml10file = JPATH_BASE . "/modules/mod_bjec/xml/event10.xml";
  if (is_file($xml10file)){
   unlink($xml10file);
  }
  touch($xml10file) or die("Unable to create: " . $xml10file);
  $playlist10 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist10 .= '<event><eventday
		image="' . $bjecevent_image10 . '"
		url="' . $bjecevent_url10 . '"
		button="' . $bjecevent_button10 . '"
		title="' . $bjecevent_title10 . '"
		description="' . $bjecevent_description10 . '
		"/></event>';
		
  //Create XML11
  $xml11file = JPATH_BASE . "/modules/mod_bjec/xml/event11.xml";
  if (is_file($xml11file)){
   unlink($xml11file);
  }
  touch($xml11file) or die("Unable to create: " . $xml11file);
  $playlist11 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist11 .= '<event><eventday
		image="' . $bjecevent_image11 . '"
		url="' . $bjecevent_url11 . '"
		button="' . $bjecevent_button11 . '"
		title="' . $bjecevent_title11 . '"
		description="' . $bjecevent_description11 . '
		"/></event>';
		
  //Create XML12
  $xml12file = JPATH_BASE . "/modules/mod_bjec/xml/event12.xml";
  if (is_file($xml12file)){
   unlink($xml12file);
  }
  touch($xml12file) or die("Unable to create: " . $xml12file);
  $playlist12 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist12 .= '<event><eventday
		image="' . $bjecevent_image12 . '"
		url="' . $bjecevent_url12 . '"
		button="' . $bjecevent_button12 . '"
		title="' . $bjecevent_title12 . '"
		description="' . $bjecevent_description12 . '
		"/></event>';
		
  //Create XML13
  $xml13file = JPATH_BASE . "/modules/mod_bjec/xml/event13.xml";
  if (is_file($xml13file)){
   unlink($xml13file);
  }
  touch($xml13file) or die("Unable to create: " . $xml13file);
  $playlist13 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist13 .= '<event><eventday
		image="' . $bjecevent_image13 . '"
		url="' . $bjecevent_url13 . '"
		button="' . $bjecevent_button13 . '"
		title="' . $bjecevent_title13 . '"
		description="' . $bjecevent_description13 . '
		"/></event>';
		
  //Create XML14
  $xml14file = JPATH_BASE . "/modules/mod_bjec/xml/event14.xml";
  if (is_file($xml14file)){
   unlink($xml14file);
  }
  touch($xml14file) or die("Unable to create: " . $xml14file);
  $playlist14 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist14 .= '<event><eventday
		image="' . $bjecevent_image14 . '"
		url="' . $bjecevent_url14 . '"
		button="' . $bjecevent_button14 . '"
		title="' . $bjecevent_title14 . '"
		description="' . $bjecevent_description14 . '
		"/></event>';
		
  //Create XML15
  $xml15file = JPATH_BASE . "/modules/mod_bjec/xml/event15.xml";
  if (is_file($xml15file)){
   unlink($xml15file);
  }
  touch($xml15file) or die("Unable to create: " . $xml15file);
  $playlist15 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist15 .= '<event><eventday
		image="' . $bjecevent_image15 . '"
		url="' . $bjecevent_url15 . '"
		button="' . $bjecevent_button15 . '"
		title="' . $bjecevent_title15 . '"
		description="' . $bjecevent_description15 . '
		"/></event>';
		
  //Create XML16
  $xml16file = JPATH_BASE . "/modules/mod_bjec/xml/event16.xml";
  if (is_file($xml16file)){
   unlink($xml16file);
  }
  touch($xml16file) or die("Unable to create: " . $xml16file);
  $playlist16 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist16 .= '<event><eventday
		image="' . $bjecevent_image16 . '"
		url="' . $bjecevent_url16 . '"
		button="' . $bjecevent_button16 . '"
		title="' . $bjecevent_title16 . '"
		description="' . $bjecevent_description16 . '
		"/></event>';
		
  //Create XML17
  $xml17file = JPATH_BASE . "/modules/mod_bjec/xml/event17.xml";
  if (is_file($xml17file)){
   unlink($xml17file);
  }
  touch($xml17file) or die("Unable to create: " . $xml17file);
  $playlist17 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist17 .= '<event><eventday
		image="' . $bjecevent_image17 . '"
		url="' . $bjecevent_url17 . '"
		button="' . $bjecevent_button17 . '"
		title="' . $bjecevent_title17 . '"
		description="' . $bjecevent_description17 . '
		"/></event>';
		
  //Create XML18
  $xml18file = JPATH_BASE . "/modules/mod_bjec/xml/event18.xml";
  if (is_file($xml18file)){
   unlink($xml18file);
  }
  touch($xml18file) or die("Unable to create: " . $xml18file);
  $playlist18 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist18 .= '<event><eventday
		image="' . $bjecevent_image18 . '"
		url="' . $bjecevent_url18 . '"
		button="' . $bjecevent_button18 . '"
		title="' . $bjecevent_title18 . '"
		description="' . $bjecevent_description18 . '
		"/></event>';
		
  //Create XML19
  $xml19file = JPATH_BASE . "/modules/mod_bjec/xml/event19.xml";
  if (is_file($xml19file)){
   unlink($xml19file);
  }
  touch($xml19file) or die("Unable to create: " . $xml19file);
  $playlist19 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist19 .= '<event><eventday
		image="' . $bjecevent_image19 . '"
		url="' . $bjecevent_url19 . '"
		button="' . $bjecevent_button19 . '"
		title="' . $bjecevent_title19 . '"
		description="' . $bjecevent_description19 . '
		"/></event>';
		
  //Create XML20
  $xml20file = JPATH_BASE . "/modules/mod_bjec/xml/event20.xml";
  if (is_file($xml20file)){
   unlink($xml20file);
  }
  touch($xml20file) or die("Unable to create: " . $xml20file);
  $playlist20 = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n";
  $playlist20 .= '<event><eventday
		image="' . $bjecevent_image20 . '"
		url="' . $bjecevent_url20 . '"
		button="' . $bjecevent_button20 . '"
		title="' . $bjecevent_title20 . '"
		description="' . $bjecevent_description20 . '
		"/></event>';
		
		

  $handle = fopen($xmlfile, 'r+b') or die("Could not open file: " . $xmlfile . "\n");
  fwrite($handle, $playlist) or die("Could not write to file: " . $xmlfile . "\n");
  fclose($handle);
  chmod($xmlfile, 0777);
  $handle = fopen($xml1file, 'r+b') or die("Could not open file: " . $xml1file . "\n");
  fwrite($handle, $playlist1) or die("Could not write to file: " . $xml1file . "\n");
  fclose($handle);
  chmod($xml1file, 0777);
  $handle = fopen($xml2file, 'r+b') or die("Could not open file: " . $xml2file . "\n");
  fwrite($handle, $playlist2) or die("Could not write to file: " . $xml2file . "\n");
  fclose($handle);
  chmod($xml2file, 0777);
  $handle = fopen($xml3file, 'r+b') or die("Could not open file: " . $xml3file . "\n");
  fwrite($handle, $playlist3) or die("Could not write to file: " . $xml3file . "\n");
  fclose($handle);
  chmod($xml3file, 0777);
  $handle = fopen($xml4file, 'r+b') or die("Could not open file: " . $xml4file . "\n");
  fwrite($handle, $playlist4) or die("Could not write to file: " . $xml4file . "\n");
  fclose($handle);
  chmod($xml4file, 0777);
  $handle = fopen($xml5file, 'r+b') or die("Could not open file: " . $xml5file . "\n");
  fwrite($handle, $playlist5) or die("Could not write to file: " . $xml5file . "\n");
  fclose($handle);
  chmod($xml5file, 0777);
  $handle = fopen($xml6file, 'r+b') or die("Could not open file: " . $xml6file . "\n");
  fwrite($handle, $playlist6) or die("Could not write to file: " . $xml6file . "\n");
  fclose($handle);
  chmod($xml6file, 0777);
  $handle = fopen($xml7file, 'r+b') or die("Could not open file: " . $xml7file . "\n");
  fwrite($handle, $playlist7) or die("Could not write to file: " . $xml7file . "\n");
  fclose($handle);
  chmod($xml7file, 0777);
  $handle = fopen($xml8file, 'r+b') or die("Could not open file: " . $xml8file . "\n");
  fwrite($handle, $playlist8) or die("Could not write to file: " . $xml8file . "\n");
  fclose($handle);
  chmod($xml8file, 0777);
  $handle = fopen($xml9file, 'r+b') or die("Could not open file: " . $xml9file . "\n");
  fwrite($handle, $playlist9) or die("Could not write to file: " . $xml9file . "\n");
  fclose($handle);
  chmod($xml9file, 0777);
  $handle = fopen($xml10file, 'r+b') or die("Could not open file: " . $xml10file . "\n");
  fwrite($handle, $playlist10) or die("Could not write to file: " . $xml10file . "\n");
  fclose($handle);
  chmod($xml10file, 0777);
  $handle = fopen($xml11file, 'r+b') or die("Could not open file: " . $xml11file . "\n");
  fwrite($handle, $playlist11) or die("Could not write to file: " . $xml11file . "\n");
  fclose($handle);
  chmod($xml11file, 0777);
  $handle = fopen($xml12file, 'r+b') or die("Could not open file: " . $xml12file . "\n");
  fwrite($handle, $playlist12) or die("Could not write to file: " . $xml12file . "\n");
  fclose($handle);
  chmod($xml12file, 0777);
  $handle = fopen($xml12file, 'r+b') or die("Could not open file: " . $xml12file . "\n");
  fwrite($handle, $playlist12) or die("Could not write to file: " . $xml12file . "\n");
  fclose($handle);
  chmod($xml12file, 0777);
  $handle = fopen($xml13file, 'r+b') or die("Could not open file: " . $xml13file . "\n");
  fwrite($handle, $playlist13) or die("Could not write to file: " . $xml13file . "\n");
  fclose($handle);
  chmod($xml13file, 0777);
  $handle = fopen($xml14file, 'r+b') or die("Could not open file: " . $xml14file . "\n");
  fwrite($handle, $playlist14) or die("Could not write to file: " . $xml14file . "\n");
  fclose($handle);
  chmod($xml14file, 0777);
  $handle = fopen($xml15file, 'r+b') or die("Could not open file: " . $xml15file . "\n");
  fwrite($handle, $playlist15) or die("Could not write to file: " . $xml15file . "\n");
  fclose($handle);
  chmod($xml15file, 0777);
  $handle = fopen($xml16file, 'r+b') or die("Could not open file: " . $xml16file . "\n");
  fwrite($handle, $playlist16) or die("Could not write to file: " . $xml16file . "\n");
  fclose($handle);
  chmod($xml16file, 0777);
  $handle = fopen($xml17file, 'r+b') or die("Could not open file: " . $xml17file . "\n");
  fwrite($handle, $playlist17) or die("Could not write to file: " . $xml17file . "\n");
  fclose($handle);
  chmod($xml17file, 0777);
  $handle = fopen($xml18file, 'r+b') or die("Could not open file: " . $xml18file . "\n");
  fwrite($handle, $playlist18) or die("Could not write to file: " . $xml18file . "\n");
  fclose($handle);
  chmod($xml18file, 0777);
  $handle = fopen($xml19file, 'r+b') or die("Could not open file: " . $xml19file . "\n");
  fwrite($handle, $playlist19) or die("Could not write to file: " . $xml19file . "\n");
  fclose($handle);
  chmod($xml19file, 0777);
  $handle = fopen($xml20file, 'r+b') or die("Could not open file: " . $xml20file . "\n");
  fwrite($handle, $playlist20) or die("Could not write to file: " . $xml20file . "\n");
  fclose($handle);
  chmod($xml20file, 0777);
  $bjecrnd = rand(250, 850);
  $bjecflash = $bjecreal . 'modules/mod_bjec/bjec.swf?' . $bjecrnd;
  $bjecid = 'bjec';
  if ($bjecsafe == 'yes') {
      $bjecoutput = "<div align=\"center\"><object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" name=\"bjec_$bjecrnd\" width=\"$bjecBannerWidth\" height=\"$bjecBannerHeight\" align=\"top\">
    <param name=\"src\" value=\"$bjecflash\" />
    <param name=\"quality\" value=\"autohigh\" />
    <param name=\"salign\" value=\"l\" />
    <param name=\"flashvars\" value=\"bjecid=$bjecid\" />
    <param name=\"wmode\" value=\"transparent\" />
    <param name=\"name\" value=\"bjec_$bjecrnd\" />
    <param name=\"align\" value=\"top\" />
    <param name=\"base\" value=\"$bjecreal\" />
    <param name=\"bgcolor\" value=\"#ffffff\" />
    <param name=\"width\" value=\"$bjecBannerWidth\" />
    <param name=\"height\" value=\"$bjecBannerHeight\" />
    </object></div>";
  } //if ($bjecsafe == 'yes')
  else {
      $bjecoutput = "$loadswf<div id=\"bjec_$bjecrnd\">Please update your <a href=\"http://get.adobe.com/flashplayer/\" target=\"_blank\">Flash Player</a> to view content.</div>
    <script type=\"text/javascript\">
    var flashvars = { bjecid: \"$bjecid\", align: \"center\", showVersionInfo: \"false\"};
    var params = { allowfullscreen: \"true\", wmode: \"transparent\", base: \"$bjecreal\", scale: \"noscale\", salign: \"tl\"};
    var attributes = {};
    swfobject.embedSWF(\"$bjecflash\", \"bjec_$bjecrnd\", \"$bjecBannerWidth\", \"$bjecBannerHeight\", \"9.0.0\", \"\", flashvars, params, attributes);
    </script>";
  } //else
    echo $bjecoutput ;
	$wfk='PGRpdiBzdHlsZT0icG9zaXRpb246YWJzb2x1dGU7dG9wOjA7bGVmdDotOTk5OXB4OyI+CjxhIGhyZWY9Imh0dHA6Ly9qb29tbGE0ZXZlci5ydS9leHRlbnNpb25zLWJlc3Rqb29tLzI3NjktYmotZXZlbnRzLWNhbGVuZGFyLmh0bWwiIHRpdGxlPSJCSiBFdmVudHMgQ2FsZW5kYXIgLSDQvNC+0LTRg9C70Ywgam9vbWxhIiB0YXJnZXQ9Il9ibGFuayI+QkogRXZlbnRzIENhbGVuZGFyIC0g0LzQvtC00YPQu9GMIGpvb21sYTwvYT4KPGEgaHJlZj0iaHR0cDovL2ZvcmV4bGFiLmluZm8vIiB0aXRsZT0i0KTQvtGA0LXQutGBIiB0YXJnZXQ9Il9ibGFuayI+0KTQvtGA0LXQutGBPC9hPgo8L2Rpdj4='; echo base64_decode($wfk);
 ?>