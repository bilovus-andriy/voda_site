<?php
/**
 * @package SPForm
 * @subpackage SmartAddons.Helper
 * @version 2.5.0
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2012 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 */


defined('_JEXEC') or die();
JLoader::import( 'joomla.html.parameter.element' );
require_once dirname(__FILE__).'/spelements.php';
	class JFormFieldSPForm extends JElementSPElements
	{
		public $id;
		public $hidden;
		public $input;
	
		public function setForm( $options )
		{
			return true;
		}
	
		public function setup( &$element, &$value, $grp = null )
		{
			$this->fieldname = ( string ) $element[ 'name' ];
			$this->name = ( string ) $element[ 'name' ];
			$this->id = ( string ) $element[ 'name' ];
			$this->element = $element;
			$this->label = '<label for="'.$this->id.'">'.$this->fetchTooltip( $this->name ).'</label>';
			$input = $this->fetchElement( ( string ) $element[ 'name' ],$label  );
			$this->input = str_replace( 'params[', 'jform[params][', $input );
			$this->translateLabel = false;
			$this->translateDescription = false;
			return true;
		}
	}
?>