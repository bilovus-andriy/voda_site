<?php
/**
 * @package Sj Extra Slider for SobiPro
 * @version 2.5.0
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2012 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 */



defined( '_JEXEC' ) or die;
SPLoader::loadController( 'section' );
SPLoader::loadView( 'section' );

require_once dirname(__FILE__).'/helper_base.php';
class SPExtraSliderHelper extends SpBaseHelper
{
	public static function getList($params){
		static $helper = null;
		if( !( $helper ) ) {
			$helper =  new self();
		}
		return $helper->_getList( $params );
	}
	
	public function _getList($params){
		$list= array();
		$eids = $this->entries($params);
		if( count( $eids ) ) {
			$helper = new SPExtraSliderHelperView();
			foreach ( $eids as $eid ) {
				$list[] = $helper->entry($eid, 0, 1);
			}
			
		}
		return $list ;
	}
	/**
	 * Get value of $field in $item
	 * @param mixed $item
	 * @param string $field
	 */

	
	public function entry(){
		$en = array();
		if ( is_numeric( $entry ) ) {
			$en = $this->cachedEntry( $entry, $manager, $noId );
		}
		if ( !( is_array( $en ) ) || !( count( $en ) ) ) {
			if ( is_numeric( $entry ) ) {
				$entry = SPFactory::Entry( $entry );
			}
			$en[ 'id' ] = $entry->get( 'id' );
			$en[ 'nid' ] = $entry->get( 'nid' );
			$en[ 'name' ] = array(
					'_complex' => 1,
					'_data' => $entry->get( 'name' ),
					'_attributes' => array( 'lang' => Sobi::Lang( false ) )
			);
			$en[ 'url_array' ] = array( 'title' => $entry->get( 'name' ), 'pid' => $entry->get( 'primary' ), 'sid' => $entry->get( 'id' ) );
			if ( strstr( SPRequest::task(), 'search' ) || $noId || ( Sobi::Cfg( 'section.force_category_id', false ) && SPRequest::sid() == Sobi::Section() ) ) {
				$en[ 'url' ] = Sobi::Url( array( 'title' => $entry->get( 'name' ), /*'task' => 'entry.details',*/
						'pid' => $entry->get( 'primary' ), 'sid' => $entry->get( 'id' ) ) );
			}
			else {
				$en[ 'url' ] = Sobi::Url( array( 'title' => $entry->get( 'name' ), /*'task' => 'entry.details',*/
						'pid' => SPRequest::sid(), 'sid' => $entry->get( 'id' ) ) );
			}
			if ( Sobi::Cfg( 'list.entry_meta', true ) ) {
				$en[ 'meta' ] = array(
						'description' => $entry->get( 'metaDesc' ),
						'keys' => $this->metaKeys( $entry ),
						'author' => $entry->get( 'metaAuthor' ),
						'robots' => $entry->get( 'metaRobots' ),
				);
			}
			if ( $manager || ( ( Sobi::My( 'id' ) && ( Sobi::My( 'id' ) == $entry->get( 'owner' ) ) && Sobi::Can( 'entry', 'edit', 'own', Sobi::Section() ) ) ) ) {
				$en[ 'edit_url' ] = Sobi::Url( array( 'task' => 'entry.edit', 'pid' => SPRequest::sid(), 'sid' => $entry->get( 'id' ) ) );
			}
			else {
				if ( isset( $en[ 'edit_url' ] ) ) {
					unset( $en[ 'edit_url' ] );
				}
			}
			$en[ 'edit_url_array' ] = array( 'task' => 'entry.edit', 'pid' => SPRequest::sid(), 'sid' => $entry->get( 'id' ) );
			$en[ 'created_time' ] = $entry->get( 'createdTime' );
			$en[ 'updated_time' ] = $entry->get( 'updatedTime' );
			$en[ 'valid_since' ] = $entry->get( 'validSince' );
			$en[ 'valid_until' ] = $entry->get( 'validUntil' );
			if ( $entry->get( 'state' ) == 0 ) {
				$en[ 'state' ] = 'unpublished';
			}
			else {
				if ( strtotime( $entry->get( 'validUntil' ) ) != 0 && strtotime( $entry->get( 'validUntil' ) ) < time() ) {
					$en[ 'state' ] = 'expired';
				}
				elseif ( strtotime( $entry->get( 'validSince' ) ) != 0 && strtotime( $entry->get( 'validSince' ) ) > time() ) {
					$en[ 'state' ] = 'pending';
				}
				else {
					$en[ 'state' ] = 'published';
				}
			}
			$en[ 'author' ] = $entry->get( 'owner' );
			$en[ 'counter' ] = $entry->get( 'counter' );
			$en[ 'approved' ] = $entry->get( 'approved' );
			//		$en[ 'confirmed' ] = $entry->get( 'confirmed' );
			if ( Sobi::Cfg( 'list.entry_cats', true ) ) {
				$cats = $entry->get( 'categories' );
				$categories = array();
				if ( count( $cats ) ) {
					$cn = SPLang::translateObject( array_keys( $cats ), 'name' );
				}
				foreach ( $cats as $cid => $cat ) {
					$categories[ ] = array(
							'_complex' => 1,
							'_data' => SPLang::clean( $cn[ $cid ][ 'value' ] ),
							'_attributes' => array( 'lang' => Sobi::Lang( false ), 'id' => $cat[ 'pid' ], 'position' => $cat[ 'position' ], 'url' => Sobi::Url( array( 'sid' => $cat[ 'pid' ], 'title' => $cat[ 'name' ] ) ) )
					);
				}
				$en[ 'categories' ] = $categories;
			}
			$fields = $entry->getFields();
			$f = array();
			if ( count( $fields ) ) {
				foreach ( $fields as $field ) {
					if ( $field->enabled( 'vcard' ) && $field->get( 'id' ) != Sobi::Cfg( 'entry.name_field' ) ) {
						$struct = $field->struct();
						$options = null;
						if ( isset( $struct[ '_options' ] ) ) {
							$options = $struct[ '_options' ];
							unset( $struct[ '_options' ] );
						}
						$f[ $field->get( 'nid' ) ] = array(
								'_complex' => 1,
								'_data' => array(
										'label' => array(
												'_complex' => 1,
												'_data' => $field->get( 'name' ),
												'_attributes' => array( 'lang' => Sobi::Lang( false ), 'show' => $field->get( 'withLabel' ) )
										),
										'data' => $struct,
								),
								'_attributes' => array( 'id' => $field->get( 'id' ), 'type' => $field->get( 'type' ), 'suffix' => $field->get( 'suffix' ), 'position' => $field->get( 'position' ), 'css_class' => ( strlen( $field->get( 'cssClass' ) ) ? $field->get( 'cssClass' ) : 'spField' ) )
						);
						if ( Sobi::Cfg( 'list.field_description', false ) ) {
							$f[ $field->get( 'nid' ) ][ '_data' ][ 'description' ] = array( '_complex' => 1, '_xml' => 1, '_data' => $field->get( 'description' ) );
						}
						if ( $options ) {
							$f[ $field->get( 'nid' ) ][ '_data' ][ 'options' ] = $options;
						}
						if ( isset( $struct[ '_xml_out' ] ) && count( $struct[ '_xml_out' ] ) ) {
							foreach ( $struct[ '_xml_out' ] as $k => $v )
								$f[ $field->get( 'nid' ) ][ '_data' ][ $k ] = $v;
						}
					}
				}
				$en[ 'fields' ] = $f;
			}
			SPFactory::cache()
			->addObj( $entry, 'entry', $entry->get( 'id' ) )
			->addObj( $en, 'entry_struct', $entry->get( 'id' ) );
			unset( $en[ 'url_array' ] );
			unset( $en[ 'edit_url_array' ] );
			unset( $entry );
		}
		/*
		 * this is te special case:
		* no matter what task we currently have - if someone called this we need the data for the V-Card
		* Soe we have to trigger all these plugins we need and therefore also fake the task
		*/
		$task = 'list.custom';
		SPFactory::registry()->set( 'task', $task );
		Sobi::Trigger( 'List', ucfirst( __FUNCTION__ ), array( &$en ) );
		return $en;
	}

	/**
	 * @return array
	 */
	private function entries( $params )
	{
		if( $params->get( 'fieldOrder' ) ) {
			$eOrder = $params->get( 'fieldOrder' );
		}
		else {
			$eOrder = $params->get( 'spOrder' );
		}
		$entriesRecursive = true;
		/* var SPDb $db */
		$db =& SPFactory::db();
		$entries = array();
		$eDir = 'asc';
		$oPrefix = null;
		$conditions = array();

		/* get the ordering and the direction */
		if( strstr( $eOrder, '.' ) ) {
			$eOrder = explode( '.', $eOrder );
			$eDir = $eOrder[ 1 ];
			$eOrder = $eOrder[ 0 ];
		}
		$pid = $params->get( 'sid' );
		$this->setModel( 'section' );
		$this->_model->init( $pid );
		$catid_filter = $params->get('catid_filter',0);
		$catids = $params->get('catid',null);
		switch ($catid_filter){
			case 0:
				$pids = $this->_model->getChilds( 'category', true );
				$conditions[ 'sprl.pid' ] = $pids;
				break;
			case 1:
				if(!empty($catids)){
					$conditions[ 'sprl.pid' ] = $catids;
				}
				break;
			case 2:
				$pids = $this->_model->getChilds( 'category', true );
				if(!empty($catids)){
					foreach($catids as  $catid){
						if(isset($pids[$catid])){
							unset($pids[$catid]);
						}
					}
				}
				if(!empty($pids)){
					$conditions[ 'sprl.pid' ] = $pids;
				}
				break;
			default:
				break;
		}
			
		if( $pid == -1 ) {
			unset( $conditions[ 'sprl.pid' ] );
		}
			
		/* sort by field */
		if( strstr( $eOrder, 'field_' ) ) {
			static $fields = array();
			$specificMethod = false;
			$field = isset( $fields[ $pid ] ) ? $fields[ $pid ] : null;
			
			if( !$field ) {
				try {
					$db->select( 'fieldType', 'spdb_field', array( 'nid' => $eOrder ) );
					$fType = $db->loadResult();
				}
				catch ( SPException $x ) {
					Sobi::Error( $this->name(), SPLang::e( 'CANNOT_DETERMINE_FIELD_TYPE', $x->getMessage() ), SPC::WARNING, 0, __LINE__, __FILE__ );
				}
				if( $fType ) {
					$field = SPLoader::loadClass( 'opt.fields.'.$fType );
				}
				$fields[ $pid ] = $field;
			}
			if( $field && method_exists( $field, 'sortBy' ) ) {
				$table = null;
				$oPrefix = null;
				$specificMethod = call_user_func_array( array( $field, 'sortBy' ), array( &$table, &$conditions, &$oPrefix, &$eOrder, &$eDir ) );
			}
			if( !$specificMethod ) {
				$table = $db->join(
					array(
						array( 'table' => 'spdb_field', 'as' => 'fdef', 'key' => 'fid' ),
						array( 'table' => 'spdb_field_data', 'as' => 'fdata', 'key' => 'fid' ),
						array( 'table' => 'spdb_object', 'as' => 'spo', 'key' => array( 'fdata.sid','spo.id' ) ),
						array( 'table' => 'spdb_relations', 'as' => 'sprl', 'key' => array( 'fdata.sid','sprl.id' )  ),
					)
				);
				$oPrefix = 'spo.';
				$conditions[ 'spo.oType' ] = 'entry';
				$conditions[ 'fdef.nid' ] = $eOrder;
				$eOrder = 'baseData.'.$eDir;
			}
		}
		else {
			$table = $db->join( array(
				array( 'table' => 'spdb_relations', 'as' => 'sprl', 'key' => 'id' ),
				array( 'table' => 'spdb_object', 'as' => 'spo', 'key' => 'id' )
			) );
			$conditions[ 'spo.oType' ] = 'entry';
			$eOrder = $eOrder.'.'.$eDir;
			$oPrefix = 'spo.';
			
		}
	
		/* check user permissions for the visibility */
		if( Sobi::My( 'id' ) ) {
			$this->userPermissionsQuery( $conditions, $oPrefix );
		}
		else {
			$conditions = array_merge( $conditions, array( $oPrefix.'state' => '1', '@VALID' => $db->valid( $oPrefix.'validUntil', $oPrefix.'validSince' ) ) );
			
		}
		
		$conditions[ 'sprl.copy' ] = '0';
		
		try {
			$db->select( $oPrefix.'id', $table, $conditions, $eOrder, $params->get( 'entriesLimit' ), 0, true );
			$results = $db->loadResultArray();
		}
		catch ( SPException $x ) {
			Sobi::Error( $this->name(), SPLang::e( 'DB_REPORTS_ERR', $x->getMessage() ), SPC::WARNING, 0, __LINE__, __FILE__ );
		}
		if( count( $results ) ) {
			foreach ( $results as $i => $sid ) {
				$entries[ $i ] = $sid;
			}
		}
		return $entries;
	}
}

class SPExtraSliderHelperView extends SPSectionView
{
	 public function __call($method, $args){
		if (is_callable(array($this, $method))){
			return call_user_func_array(array($this, $method), $args);
		}
		return null;
	} 
}

