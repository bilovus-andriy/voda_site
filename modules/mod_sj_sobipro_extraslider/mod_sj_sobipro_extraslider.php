<?php
/**
 * @package Sj Extra Slider for SobiPro
 * @version 2.5.0
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2012 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 */


defined('_JEXEC') or die;
if( file_exists(implode( DS, array( JPATH_ROOT, 'components', 'com_sobipro', 'lib', 'sobi.php' ) ))){
	require_once ( implode( DS, array( JPATH_ROOT, 'components', 'com_sobipro', 'lib', 'sobi.php' ) ) );
	
	Sobi::Init( JPATH_ROOT, JFactory::getConfig()->getValue( 'config.language' ), $params->get( 'sid' ) );
	
	require_once dirname( __FILE__ ).'/core/helper.php';
	$layout = $params->get('layout','default');
	$cacheid = md5(serialize(array ($layout, $module->module)));
	$cacheparams = new stdClass;
	$cacheparams->cachemode = 'id';
	$cacheparams->class = 'SPExtraSliderHelper';
	$cacheparams->method = 'getList';
	$cacheparams->methodparams = $params;
	$cacheparams->modeparams = $cacheid;
	$entries = JModuleHelper::moduleCache ($module, $params, $cacheparams);
	require JModuleHelper::getLayoutPath($module->module,$layout);
}else{
	echo JText::_('Has no content to show!');
}