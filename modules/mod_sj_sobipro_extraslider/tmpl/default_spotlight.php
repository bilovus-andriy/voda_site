<?php
/**
 * @package Sj Extra Slider for SobiPro
 * @version 2.5.0
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2012 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 */
	defined('_JEXEC') or die;
 ?>

    <div id="<?php echo $uniqued; ?>" class="extraslider-wrapper">
	    <?php  if ($params->get('pretext', '') != ''){ ?>
	    <div class="pretext">
	        <?php  echo $params->get('pretext');?>
	    </div>
	    <?php }?>
        <div class="extraslider-heading">
            <?php if($params->get('title_slider_display') == 1){?>
            <div class="heading-title"><?php echo $params->get('title_slider');?></div><!--end heading-title-->
            <?php }
            if($params->get('prenext_show') == 1){
            ?>
            <div class="extraslider-control  <?php if($params->get('button_page') =='under'){echo "button-padding";}?>">
    			<ul class="page-paging" style="margin:0px; padding:0px;">
    				<li id="previous_<?php echo $module->id ?>" class="previous <?php if($params->get('button_page') =='under'){echo "style2";} ?>"></li>
                    <?php if($params->get('button_page') =='top'){?>
                        <li id="paging_<?php echo $module->id;?>" class="paging"></li>
                    <?php }?>
    				<li id="next_<?php echo $module->id ?>" class="next <?php if($params->get('button_page') =='under'){echo "style2";} ?>"></li>
    			</ul>
            </div><!--end extraslider-control-->
            <?php }?>
        </div><!--end extraslider-heading-->
    
    
    <div class="extraslider-items">
        <div id="extraslider_items_inner_<?php echo $module->id ?>" class="extraslider-items-inner">
        <?php $count = 0;
         foreach ($entries as $entry){ $count++;?>
        <?php if($count%$item_of_page == 1 || $item_of_page == 1){?>
            <div class="page">
        <?php }?>
                <?php if($count%$params->get('num_cols') == 1 || $params->get('num_cols') == 1 ){?>
                <div class="line">
                <?php }?>
                    <div class="item-wrapper <?php if($count%$params->get('num_cols') == 0){echo "padding-right";} ?>">
                        <div class="item-image item-image-spotlight">
                            <?php if($params->get('item_image_display') == 1){ ?>
								<a href="<?php echo $entry['url']; ?>" title="<?php echo $entry['name']['_data']; ?>" <?php echo SPExtraSliderHelper::parseTarget($params->get('item_link_target')); ?> >
									<?php $img = SPExtraSliderHelper::getSPImage($entry, $params);
										echo SPExtraSliderHelper::imageTag($img);?>
								</a>
                            <?php }?>
                            <div class="item-info item-info-spotlight">
                                <div class="item-spotlight">
                                <?php if($params->get('item_image_display') == 1){?>
                                    <div class="item-title" >
									   <a href="<?php echo $entry['url']; ?>" title="<?php echo $entry['name']['_data']; ?>" <?php echo SPExtraSliderHelper::parseTarget($params->get('item_link_target')); ?> >
										<?php echo SPExtraSliderHelper::truncate($entry['name']['_data'], $params->get('item_title_max_characs'))?>
										</a>
                                    </div><!--end item-title-->
                                <?php }
								$desc = SPExtraSliderHelper::_cleanText(SPExtraSliderHelper::getFieldValue($entry,$params->get('field_description')));
                                if($params->get('item_desc_display') ==1 && !empty($desc)){?>
									<div class="item-description">
										<?php echo SPExtraSliderHelper::truncate($desc, $params->get('item_desc_max_characs')); ?>
									</div><!--end item-description-->
                                <?php }?>
                                
								<?php if($params->get('item_price_display') == 1){ ?>
           							<div class="item-price">
           								<?php echo  SPExtraSliderHelper::getFieldValue($entry, $params->get('field_price')); ?>
           							</div>
           						<?php } ?>                                 
                                
                                <?php if($params->get('item_readmore_display') == 1){?>
                                    <div class="item-readmore">
										<a href="<?php echo $entry['url']; ?>" title="<?php echo $entry['name']['_data']; ?>" <?php echo SPExtraSliderHelper::parseTarget($params->get('item_link_target')); ?> >
											<?php echo $params->get('item_readmore_text', 'Read more'); ?>
										</a>
									</div><!--end item-readmore-->
                                <?php }?>
                                </div>
                                <div class="opacity"></div>
                            </div><!--end item-info-->
                        </div><!--end item-image-->
                    </div><!--item-wrapper-->
                <?php if($count%$params->get('num_cols') == 0 || $count== $count_item){?>    
                </div><!--line-->
                <?php } ?>
            <?php if(($count%$item_of_page == 0 || $count== $count_item)){?>    
            </div><!--end page--> 
        <?php }}?>             
        </div><!--end extraslider_items_inner-->    
    </div><!--end extraslider-items-->


    <?php 
    if($params->get('prenext_show') ==1){
    if($params->get('button_page') =='under'){ ?>
        <div class="extraslider-control-under">
    		<ul class="page-paging" style="margin:0px; padding:10px 0 0 0px;">
    			<li id="paging_<?php echo $module->id;?>" class="paging"></li>
    		</ul>
        </div><!--end page-paging-under-->
    <?php }}?>
     <?php if ($params->get('posttext', '') != ''): ?>
	    <div>
	        <?php  echo $params->get('posttext');?>
	    </div>
	<?php endif; ?>
    </div><!--end sj_extra_wrapper-->
    
	<script type="text/javascript">
	    jQuery(document).ready(function($){
	    	$('#<?php echo $uniqued; ?>').imagesLoaded(function(){
				$('#extraslider_items_inner_<?php echo $module->id;?>','#<?php echo $uniqued; ?>').carouFredSel({
						prev: '#previous_<?php echo $module->id;?>',
						next: '#next_<?php echo $module->id;?>',
						pagination: "#paging_<?php echo $module->id;?>",
			            direction :'<?php if($params->get('effect') =="vertical"){echo 'up';}else{echo 'left';} ?>',
			            <?php if($params->get('play') == 1){ ?>
						auto: {
			                pauseOnHover: <?php if($params->get('pause_hover') == 1){echo "true";}else {echo "false";} ?>,
			                duration : 500
			            }
			            <?php }else {?>
			            auto:false
			            <?php }?>
					});
		    	});
	    });
	</script>