<?php
/**
 * @package Sj Extra Slider for SobiPro
 * @version 2.5.0
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2012 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 */

defined('_JEXEC') or die;

if (count($entries)){

	JHtml::stylesheet('modules/'.$module->module.'/assets/css/style-font-color.css');
	JHtml::stylesheet('modules/'.$module->module.'/assets/css/style.css');
	if( !defined('SMART_JQUERY') && $params->get('include_jquery', 0) == "1" ){
		JHtml::script('modules/'.$module->module.'/assets/js/jquery-1.8.2.min.js');
		JHtml::script('modules/'.$module->module.'/assets/js/jquery-noconflict.js');
		define('SMART_JQUERY', 1);
	}

	JHtml::script('modules/'.$module->module.'/assets/js/jquery.carouFredSel.js');
	JHtml::script('modules/'.$module->module.'/assets/js/jquery.imagesloaded.js');
	
	ImageHelper::setDefault($params);
	
	$uniqued='extraslider'.rand().time();
	$width_image = $params->get('imgcfg_width')+11;
	if((int)$params->get('num_rows') <= 0){
		$params->set('num_rows',1);
	}
	if((int)$params->get('num_cols') <= 0){
		$params->set('num_cols',1) ;
	}
	
	$width_module = $width_image*$params->get('num_cols')-11;
	$count_item = count($entries);
	$item_of_page = $params->get('num_rows') * $params->get('num_cols');
	if($count_item < $item_of_page){
		$item_of_page == $count_item;
	}//var_dump($item_of_page); die("abc");
	$num_page = ceil($count_item/$item_of_page);
	$padding_page_width = ($num_page*12)-3;
	$padding_page_left = ($width_module - $padding_page_width)/2;
	/*-- css --*/
	ob_start();
	?>
	<?php echo '#'.$uniqued; ?>{
	    width:<?php echo $width_module;?>px;
	}
	<?php echo '#'.$uniqued; ?> .extraslider-heading{
	    width:<?php echo $width_module;?>px;
	}
	<?php echo '#'.$uniqued; ?> .extraslider-items{
	    width:<?php echo $width_module;?>px;
	}
	<?php echo '#'.$uniqued; ?> .page{
	    width:<?php echo $width_module;?>px;
	}
	<?php echo '#'.$uniqued; ?> .page .line{
	    width:<?php  echo $width_module;?>px;
	}
	<?php echo '#'.$uniqued; ?> .page .line .item-wrapper{
	    width:<?php echo $params->get('imgcfg_width');?>px;
	}
	<?php echo '#'.$uniqued; ?> .page .line .item-wrapper .item-image{
	    width:<?php echo $params->get('imgcfg_width');?>px;
	    height:<?php echo $options->item_image_height;?>px;
	    overflow:hidden
	}
	<?php echo '#'.$uniqued; ?> .page .line .item-wrapper .item-info.item-info-spotlight .opacity{
	    width:<?php echo $params->get('imgcfg_width');?>px;
	}
	<?php echo '#'.$uniqued; ?> .extraslider-control-under{
	    width:<?php echo $width_module;?>px;
	}
	<?php echo '#'.$uniqued; ?> .extraslider-control-under .page-paging{
	    padding:10px 0 0 <?php echo $padding_page_left;?>px;
	}
	
	<?php 
	$style_sheet = ob_get_contents();
	@ob_end_clean();
	$document = &JFactory::getDocument();
	$document->addStyleDeclaration($style_sheet);
	
	$layoutname = $params->get('theme', 'basic');
	require JModuleHelper::getLayoutPath($module->module, 'default_'.$layoutname);
	
} else {
	echo JText::_('Has no content to show!');
}
?>