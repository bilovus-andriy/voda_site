var jx = jQuery.noConflict();
(function($jx){

	var current = null; 
	
	$jx.fn.rssfeed = function(url, options) {	
	
		// Set plugin defaults
		var defaults = {
			limit: 6,
			titletag: 'h4',
			content: true,
			snippet: true,
			showerror: true,
			errormsg: '',
			key: null
		};  
		var options = $jx.extend(defaults, options); 
		
		// Functions
		return this.each(function(i, e) {
			var $jxe = $jx(e);
			
			// Add feed class to user div
			if (!$jxe.hasClass('rssFeed')) $jxe.addClass('rssFeed');
			
			// Check for valid url
			if(url == null) return false;

			// Create Google Feed API address
			var api = "http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&callback=?&q=" + url;
			if (options.limit != null) api += "&num=" + options.limit;
			if (options.key != null) api += "&key=" + options.key;

			// Send request
			$jx.getJSON(api, function(data){
				
				// Check for error
				if (data.responseStatus == 200) {
	
					// Process the feeds
					_callback(e, data.responseData.feed, options);
				} else {

					// Handle error if required
					if (options.showerror)
						if (options.errormsg != '') {
							var msg = options.errormsg;
						} else {
							var msg = data.responseDetails;
						};
						$jx(e).html('<div class="rssError"><p>'+ msg +'</p></div>');
				};
			});				
		});
	};

	
	// Callback function to create HTML result
	var _callback = function(e, feeds, options) {
		if (!feeds) {
			return false;
		}
		var html = '';	
		var row = 'odd';	
		
		
			
		// Add body
		html += '<div class="rssBody">' +
			'<ul>';
		
		// Add feeds
		for (var i=0; i<feeds.entries.length; i++) {
			
			// Get individual feed
			var entry = feeds.entries[i];
			

			// Add feed row
			html += '<li class="rssRow '+row+'">' + 
				'<'+ options.titletag +'><a href="'+ entry.link +'" title="View this feed at '+ feeds.title +'">'+ entry.title +'</a></'+ options.titletag +'>'
			if (options.date) html += '<div>'+ pubDate +'</div>'
			if (options.content) {
			
				// Use feed snippet if available and optioned
				if (options.snippet && entry.contentSnippet != '') {
					var content = entry.contentSnippet;
				} else {
					var content = entry.content;
				}
				
				html += '<p><a href="'+entry.link+'" title="'+ entry.title +' " target="_blank">'+ content +'</a></p>'
			}
			
			html += '</li>';

			
			// Alternate row classes
			if (row == 'odd') {
				row = 'even';
			} else {
				row = 'odd';
			}			
		}
		
		html += '</ul>' +
			'</div>'

		
		$jx(e).html(html);

		//correct href for images so they point to Pinterest
			$jx(function() {
  			$jx('.rssFeed a').each(function() {
    			var href = $jx(this).attr('href');
    			$jx(this).attr('href', 'http://www.pinterest.com' + href);
				$jx(this).attr('target', '_blank');
  });
});

           
	};
})(jQuery);