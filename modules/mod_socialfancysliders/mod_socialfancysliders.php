<?php
/**
* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$theme_style = $params->get( 'theme_style', 0 );
$top_margin = $params->get( 'top_margin', 0 );

$jquery_load = $params->get( 'jquery_load', 0 );
$conflict_load = $params->get( 'conflict_load', 0 );

$facebook_s_enable = $params->get( 'facebook_s_enable', 0 );
$facebook_direction = $params->get( 'facebook_direction', 0 );
$facebook_top_margin = $params->get( 'facebook_top_margin', 0 );
$facebook_box_top_margin = $params->get( 'facebook_box_top_margin', 0 );
$facebook_page_url = $params->get( 'facebook_page_url', 0 );
$facebook_lang = $params->get( 'facebook_lang', 0 );
$facebook_slide_width = $params->get( 'facebook_slide_width', 0 );
$facebook_slide_height = $params->get( 'facebook_slide_height', 0 );
$facebook_faces = $params->get( 'facebook_faces', 0 );
$facebook_header = $params->get( 'facebook_header', 0 );
$facebook_stream = $params->get( 'facebook_stream', 0 );
$facebook_border_size = $params->get( 'facebook_border_size', 0 );
$facebook_border_color = $params->get( 'facebook_border_color', 0 );
$facebook_background_color = $params->get( 'facebook_background_color', 0 );

$twitter_s_enable = $params->get( 'twitter_s_enable', 0 );
$twitter_direction = $params->get( 'twitter_direction', 0 );
$twitter_top_margin = $params->get( 'twitter_top_margin', 0 );
$twitter_box_top_margin = $params->get( 'twitter_box_top_margin', 0 );
$twitter_slide_width = $params->get( 'twitter_slide_width', 0 );
$twitter_slide_height = $params->get( 'twitter_slide_height', 0 );
$twitter_username = $params->get( 'twitter_username', 0 );
$twitter_lang = $params->get( 'twitter_lang', 0 );
$twitter_twitts_number = $params->get( 'twitter_twitts_number', 0 );
$twitter_txt_color = $params->get( 'twitter_txt_color', 0 );
$twitter_link_color = $params->get( 'twitter_link_color', 0 );

$twitter_border_size = $params->get( 'twitter_border_size', 0 );
$twitter_border_color = $params->get( 'twitter_border_color', 0 );
$twitter_background_color = $params->get( 'twitter_background_color', 0 );

$gplus_s_enable = $params->get( 'gplus_s_enable', 0 );
$gplus_direction = $params->get( 'gplus_direction', 0 );
$gplus_top_margin = $params->get( 'gplus_top_margin', 0 );
$gplus_box_top_margin = $params->get( 'gplus_box_top_margin', 0 );
$gplus_slide_width = $params->get( 'gplus_slide_width', 0 );
$gplus_slide_height = $params->get( 'gplus_slide_height', 0 );
$gplus_id = $params->get( 'gplus_id', 0 );
$gplus_lang = $params->get( 'gplus_lang', 0 );
$gplus_border_size = $params->get( 'gplus_border_size', 0 );
$gplus_border_color = $params->get( 'gplus_border_color', 0 );
$gplus_background_color = $params->get( 'gplus_background_color', 0 );

$ytube_s_enable = $params->get( 'ytube_s_enable', 0 );
$ytube_direction = $params->get( 'ytube_direction', 0 );
$ytube_top_margin = $params->get( 'ytube_top_margin', 0 );
$ytube_box_top_margin = $params->get( 'ytube_box_top_margin', 0 );
$ytube_slide_width = $params->get( 'ytube_slide_width', 0 );
$ytube_slide_height = $params->get( 'ytube_slide_height', 0 );
$ytube_id = $params->get( 'ytube_id', 0 );
$ytube_border_size = $params->get( 'ytube_border_size', 0 );
$ytube_border_color = $params->get( 'ytube_border_color', 0 );
$ytube_background_color = $params->get( 'ytube_background_color', 0 );

$linkedin_s_enable = $params->get( 'linkedin_s_enable', 0 );
$linkedin_direction = $params->get( 'linkedin_direction', 0 );
$linkedin_top_margin = $params->get( 'linkedin_top_margin', 0 );
$linkedin_box_top_margin = $params->get( 'linkedin_box_top_margin', 0 );
$linkedin_slide_width = $params->get( 'linkedin_slide_width', 0 );
$linkedin_slide_height = $params->get( 'linkedin_slide_height', 0 );
$linkedin_public_profile = $params->get( 'linkedin_public_profile', 0 );
$linkedin_public_profile_enable = $params->get( 'linkedin_public_profile_enable', 0 );
$linkedin_company_profile = $params->get( 'linkedin_company_profile', 0 );
$linkedin_company_profile_enable = $params->get( 'linkedin_company_profile_enable', 0 );
$linkedin_border_size = $params->get( 'linkedin_border_size', 0 );
$linkedin_border_color = $params->get( 'linkedin_border_color', 0 );
$linkedin_background_color = $params->get( 'linkedin_background_color', 0 );

$pinterest_s_enable = $params->get( 'pinterest_s_enable', 0 );
$pinterest_direction = $params->get( 'pinterest_direction', 0 );
$pinterest_top_margin = $params->get( 'pinterest_top_margin', 0 );
$pinterest_box_top_margin = $params->get( 'pinterest_box_top_margin', 0 );
$pinterest_slide_width = $params->get( 'pinterest_slide_width', 0 );
$pinterest_slide_height = $params->get( 'pinterest_slide_height', 0 );
$pinterest_id = $params->get( 'pinterest_id', 0 );
$num_of_pins = $params->get( 'num_of_pins', 0 );
$pinterest_border_size = $params->get( 'pinterest_border_size', 0 );
$pinterest_border_color = $params->get( 'pinterest_border_color', 0 );
$pinterest_background_color = $params->get( 'pinterest_background_color', 0 );


$rssfeed_s_enable = $params->get( 'rssfeed_s_enable', 0 );
$rssfeed_direction = $params->get( 'rssfeed_direction', 0 );
$rssfeed_top_margin = $params->get( 'rssfeed_top_margin', 0 );
$rssfeed_box_top_margin = $params->get( 'rssfeed_box_top_margin', 0 );
$rssfeed_slide_width = $params->get( 'rssfeed_slide_width', 0 );
$rssfeed_slide_height = $params->get( 'rssfeed_slide_height', 0 );
$rssfeed_url = $params->get( 'rssfeed_url', 0 );
$no_of_feed = $params->get( 'no_of_feed', 0 );
$facebook_enable = $params->get( 'facebook_enable', 0 );
$twitter_enable = $params->get( 'twitter_enable', 0 );
$images_enable = $params->get( 'images_enable', 0 );

$font_family = $params->get( 'font_family', 0 );
$feed_bg_color = $params->get( 'feed_bg_color', 0 );
$bg_shadow_color = $params->get( 'bg_shadow_color', 0 );
$feed_border_color = $params->get( 'feed_border_color', 0 );
$feed_title_font_size = $params->get( 'feed_title_font_size', 0 );
$feed_title_font_color = $params->get( 'feed_title_font_color', 0 );
$feed_date_font_size = $params->get( 'feed_date_font_size', 0 );
$feed_desc_font_size = $params->get( 'feed_desc_font_size', 0 );
$feed_desc_font_color = $params->get( 'feed_desc_font_color', 0 );


$rssfeed_border_size = $params->get( 'rssfeed_border_size', 0 );
$rssfeed_border_color = $params->get( 'rssfeed_border_color', 0 );
$rssfeed_background_color = $params->get( 'rssfeed_background_color', 0 );

?>
	
<?php
$content = modSocialFancyslidersHelper::getStart( $params );
require( JModuleHelper::getLayoutPath( 'mod_socialfancysliders' ) );