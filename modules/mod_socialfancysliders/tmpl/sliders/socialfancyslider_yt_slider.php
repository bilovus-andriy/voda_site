<?php
/**
* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
**/

// no direct access
global $wpdb;
if($options['YT_TabPosition']=='Middle' && in_array($options['YT_TabDesign'],array(3,6)))
{
	$socialfancysliderHead_YT_position='top: 50%; margin-top: -30px;';
}
if($options['YT_TabPosition']=='Middle' && in_array($options['YT_TabDesign'],array(1,2,4,5)))
{
	$socialfancysliderHead_YT_position='top: 50%; margin-top: -78px;';
}
if($options['YT_TabPosition']=='Middle' && in_array($options['YT_TabDesign'],array(7,8)))
{
	$socialfancysliderHead_YT_position='top: 50%; margin-top: -45px;';
}
if($options['YT_TabPosition']=='Middle' && in_array($options['YT_TabDesign'],array(9)))
{
	$socialfancysliderHead_YT_position='top: 50%; margin-top: -18px;';
}
if($options['YT_TabPosition']=='Top')
{
	$socialfancysliderHead_YT_position='top: 5px;';
}
if($options['YT_TabPosition']=='Bottom')
{	
	$socialfancysliderHead_YT_position='bottom: 5px;';
}
if($options['YT_TabPosition']=='Fixed')
{	
	$socialfancysliderHead_YT_position='top: '.$options['YT_TabPositionPx'].'px;';
}
?>
<div class="socialfancysliderCenterOuter socialfancysliderCenterOuterYt <?php echo ($options['YT_VPosition'] == 'Fixed' ? 'socialfancysliderFixed' : '') ?> socialfancyslider<?php echo $options['YT_Position'] ?>" style="margin-top:<?php echo $options['YT_Box_TabPositionPx'];?>px; <?php echo ($options['YT_Position'] == 'Left' ? 'left: -' . ($options['YT_Width'] + $options['YT_Border']) . 'px;' : 'right: -' . ($options['YT_Width'] + $options['YT_Border']) . 'px;') ?><?php echo ($options['YT_ZIndex'] ? 'z-index: ' . $options['YT_ZIndex'] . ';' : '') ?>;">


	<div class="socialfancysliderCenterInner">
		<div class="socialfancysliderWrap socialfancysliderTheme0 socialfancysliderTab<?php echo $options['YT_TabDesign'] ?>">
			<div class="socialfancysliderForm" style="background: <?php echo $options['YT_BorderColor'] ?>; height: <?php echo $options['YT_Height'] ?>px; width: <?php echo $options['YT_Width'] ?>px; padding: <?php echo ($options['YT_Position'] == 'Left' ? $options['YT_Border'] . 'px ' . $options['YT_Border'] . 'px ' . $options['YT_Border'] . 'px 0' : $options['YT_Border'] . 'px 0 ' . $options['YT_Border'] . 'px ' . $options['YT_Border'] . 'px') ?>;">
				<h2 class="socialfancysliderHead" style="<?php echo $socialfancysliderHead_YT_position; ?> <?php echo ($options['YT_Position'] == 'Left' ? 'left: ' . ($options['YT_Width'] + $options['YT_Border']) . 'px;' : 'right: ' . ($options['YT_Width'] + $options['YT_Border']) . 'px;') ?>">YouTube</h2>
				<div class="socialfancysliderInner" style="background: <?php echo $options['YT_BackgroundColor'] ?>; height: <?php echo $options['YT_Height'] ?>px;">				
					<div style="overflow: hidden; height: 98px;">
						<iframe id="socialfancysliderYTS" src="http://www.youtube.com/subscribe_widget?p=<?php echo $options['YT_Channel']; ?>" style="overflow: hidden; height: 98px; width:100%; border: 0;" scrolling="no" frameBorder="0"></iframe>
					</div>
					<div id="socialfancysliderInnerYt" class="socialfancysliderInnerLoading" style="overflow-y: scroll; overflow-x: hidden; height: <?php echo $options['YT_Height']-98?>px;">
						<ul id="socialfancysliderYtList" class="socialfancysliderList" style="height: <?php echo $options['YT_Height']-98 ?>px">
						</ul>
						<script type="text/javascript">
						function __socialfancyslider_YTGet(data) 
						{
							var MonthNames=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
							if(!data.feed.entry)
							{
							}
							else
							{
								jQuery.each(data.feed.entry, function(i,e) {
									added=new Date(e.published.$t);
									jQuery('#socialfancysliderYtList').append('<li>' +
									'<a href="' + e.link[0].href +'" class="socialfancysliderthumb-link" target="_blank"><img src="' + e.media$group.media$thumbnail[1].url + '" alt="" width="61" height="45" class="socialfancysliderthumb" /></a>' +
									'<div class="socialfancysliderbd">' +
									'<a href="' + e.link[0].href +'" class="socialfancyslidertitle" target="_blank">' + e.title.$t + '</a>' +
									'<span class="socialfancysliderinfo">' + (!e.yt$statistics ? '' : 'views: ' + e.yt$statistics.viewCount + ' |' ) + ' added: ' + (added.getDate()) + ' ' + MonthNames[added.getMonth()] + ' ' + added.getFullYear() + '</span>' +
									'</div>' +
									'</li>');
								});
							}
							jQuery('#socialfancysliderInnerYt').removeClass('socialfancysliderInnerLoading');
						}
						jQuery(document).ready(function(){
							jQuery.getScript("http://gdata.youtube.com/feeds/users/<?php echo $options['YT_Channel']; ?>/uploads?alt=json-in-script&max-results=10&format=5&callback=__socialfancyslider_YTGet");
						});
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
</div>