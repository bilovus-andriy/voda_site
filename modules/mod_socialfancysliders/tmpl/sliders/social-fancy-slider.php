<?php
/**
* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
**/

// no direct access
require_once(dirname(__FILE__) . '/common.inc.php');


DEFINE('FBLB_DEMO',true);

	


?>


<?php

	//print_r($_REQUEST);

	global $socialfancyslider_preview_options;

		require_once(dirname(__FILE__) . '/config.php');
		
		$options = $socialfancyslider_options;
		if($options['DisableByGetParamN'] && $options['DisableByGetParamV'] && $_GET[$options['DisableByGetParamN']]==$options['DisableByGetParamV'])
		{
			return;
		}
	
	if ($options['Enable'] == 1 && $options['FacebookPageURL'])
	{
		require_once(dirname(__FILE__) . '/socialfancyslider_slider.php');
	}
	if ($options['TW_Enable'] == 1 && $options['TW_Username'])
	{
		require_once(dirname(__FILE__) . '/socialfancyslider_tw_slider.php');
	}
	
	if ($options['RS_Enable'])
	{
	
		require_once(dirname(__FILE__) . '/socialfancyslider_rs_slider.php');
	}
	
	if ($options['LI_Enable'])
	{
		require_once(dirname(__FILE__) . '/socialfancyslider_li_slider.php');
	}
	
	if ($options['GP_Enable'] == 1 && $options['GP_PageID'])
	{
		require_once(dirname(__FILE__) . '/socialfancyslider_gp_slider.php');
	}
	if ($options['YT_Enable'] == 1 && $options['YT_Channel'])
	{
		require_once(dirname(__FILE__) . '/socialfancyslider_yt_slider.php');
	}
	

	
	
	
	
	if ($options['PI_Enable'])
	{
		require_once(dirname(__FILE__) . '/socialfancyslider_pi_slider.php');
	}
	
	

?>