<?php
/**
* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
**/

global $wpdb;
if($options['PI_TabPosition']=='Middle' && in_array($options['PI_TabDesign'],array(3,6)))
{
	$socialfancysliderHead_PI_position='top: 50%; margin-top: -30px;';
}
if($options['PI_TabPosition']=='Middle' && in_array($options['PI_TabDesign'],array(1,2,4,5)))
{
	$socialfancysliderHead_PI_position='top: 50%; margin-top: -78px;';
}
if($options['PI_TabPosition']=='Middle' && in_array($options['PI_TabDesign'],array(7,8)))
{
	$socialfancysliderHead_PI_position='top: 50%; margin-top: -45px;';
}
if($options['PI_TabPosition']=='Middle' && in_array($options['PI_TabDesign'],array(9,10)))
{
	$socialfancysliderHead_PI_position='top: 50%; margin-top: -18px;';
}
if($options['PI_TabPosition']=='Middle' && in_array($options['PI_TabDesign'],array(11,13)))
{
	$socialfancysliderHead_PI_position='top: 50%; margin-top: -54px;';
}
if($options['PI_TabPosition']=='Middle' && in_array($options['PI_TabDesign'],array(12,14)))
{
	$socialfancysliderHead_PI_position='top: 50%; margin-top: -39px;';
}
if($options['PI_TabPosition']=='Top')
{
	$socialfancysliderHead_PI_position='top: 5px;';
}
if($options['PI_TabPosition']=='Bottom')
{	
	$socialfancysliderHead_PI_position='bottom: 5px;';
}
if($options['PI_TabPosition']=='Fixed')
{	
	$socialfancysliderHead_PI_position='top: '.$options['PI_TabPositionPx'].'px;';
}



?>


<script src="<?php echo JURI::root(); ?>modules/mod_socialfancysliders/js/pinterest.js"></script>
<script type="text/javascript">jQuery.noConflict();</script>
<!--<link href="<?php //echo JURI::root(); ?>modules/mod_socialfancysliders/js/style_pin.css" rel="stylesheet" type="text/css" />-->


<style>
.rssFeed {
overflow:scroll;
height:<?php echo $options['PI_Height']-75?>px;
overflow-x:hidden;
}
.rssRow h4 {
	display: none;
}

.rssBody li { 
border-top:1px solid #666666;
padding:10px;
list-style:none;


}

</style>

<div class="socialfancysliderCenterOuter socialfancysliderCenterOuterPi <?php echo ($options['PI_VPosition']=='Fixed' ? 'socialfancysliderFixed': '')?> socialfancyslider<?php echo $options['PI_Position']?>" style="margin-top:<?php echo $options['PI_Box_TabPositionPx'];?>px; <?php echo ($options['PI_Position']=='Left' ? 'left: -'.($options['PI_Width']+$options['PI_Border']).'px;' : 'right: -'.($options['PI_Width']+$options['PI_Border']).'px;')?><?php echo ($options['PI_ZIndex'] ? 'z-index: '.$options['PI_ZIndex'].';': '')?>">

	<div class="socialfancysliderCenterInner">
		<div class="socialfancysliderWrap socialfancysliderTheme0 socialfancysliderTab<?php echo $options['PI_TabDesign']?>">
			<div class="socialfancysliderForm" style="background: <?php echo $options['PI_BorderColor']?>; height: <?php echo $options['PI_Height']?>px; width: <?php echo $options['PI_Width']?>px; padding: <?php echo ($options['PI_Position']=='Left' ? $options['PI_Border'].'px '.$options['PI_Border'].'px '.$options['PI_Border'].'px 0' : $options['PI_Border'].'px 0 '.$options['PI_Border'].'px '.$options['PI_Border'].'px')?>;">

<h2 class="socialfancysliderHead" style="<?php echo $socialfancysliderHead_PI_position; ?> <?php echo ($options['PI_Position']=='Left' ? 'left: '.($options['PI_Width']+$options['PI_Border']).'px;' : 'right: '.($options['PI_Width']+$options['PI_Border']).'px;')?>">Facebook</h2>

<div class="socialfancysliderInner" style="overflow: hidden; background: <?php echo $options['PI_BackgroundColor'] ?>; height: <?php echo $options['PI_Height'] ?>px;">				

<div class="scroll"></div>
        <div id="susnetpinterestleft" >
			
            <div class="susnetpinterestinner"><div class="susnetpinterestbutton<?php echo $options['PI_Position']; ?>">
			
			
			<div id="pinterestbutton1<?php echo $options['PI_Position'];?>" style="">

			
			 <div id="pinterest-frame-r" >



<div class="pinbutton" style="padding:10px;">
<a href="http://pinterest.com/<?php echo $options['PI_PageID']; ?>/"><img src="http://passets-ec.pinterest.com/images/about/buttons/follow-me-on-pinterest-button.png" width="169" height="28" alt="Follow Me on Pinterest" /></a>
</div>

<div class="latestpins" style="color:<?php echo $params->get('textcolor'); ?>; margin-left:10px; margin-bottom:10px;"><strong>Pins for <?php echo $options['PI_PageID']; ?></strong></div>
<div id="pinterest" style="padding-left:10px;"></div>

		</div>	
		
	</div></div></div>
	


        
        </div>




</div></div></div></div></div>




<script type="text/javascript">
		jQuery(document).ready(function($) {
                $('#susnetpinterestleft > div').hover(
                    function () {
                        $('.susnetpinterestbuttonleft',$(this)).stop().animate({'marginLeft':'-12px'},400);
                    },
                    function () {
                        $('.susnetpinterestbuttonleft',$(this)).stop().animate({'marginLeft':'-305px'},400);
                    }
                );
				
				$('#pinterest').rssfeed('http://www.pinterest.com/<?php echo $options['PI_PageID']; ?>/feed.rss', {
		limit: <?php echo $options['PI_num_of_pins']; ?>,
		snippet: false,
		header: false,
		date: false
	});


                $('#susnetpinterestright > div').hover(
                    function () {
                        $('.susnetpinterestbuttonright',$(this)).stop().animate({'marginLeft':'-290px'},400);
                    },
                    function () {
                        $('.susnetpinterestbuttonright',$(this)).stop().animate({'marginLeft':'8px'},400);
                    }
                );
			}); 
</script>
