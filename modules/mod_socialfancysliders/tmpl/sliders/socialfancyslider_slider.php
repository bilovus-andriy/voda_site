<?php

/**

* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya

 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html

 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant

 * to the GNU General Public License, and as distributed it includes or

 * is derivative of works licensed under the GNU General Public License or

 * other free or open source software licenses.

**/



// no direct access

if($options['TabPosition']=='Middle' && in_array($options['TabDesign'],array(3,6)))

{

	$socialfancysliderHead_position='top: 50%; margin-top: -30px;';

}

if($options['TabPosition']=='Middle' && in_array($options['TabDesign'],array(1,2,4,5)))

{

	$socialfancysliderHead_position='top: 50%; margin-top: -78px;';

}

if($options['TabPosition']=='Middle' && in_array($options['TabDesign'],array(7,8)))

{

	$socialfancysliderHead_position='top: 50%; margin-top: -45px;';

}

if($options['TabPosition']=='Middle' && in_array($options['TabDesign'],array(9)))

{

	$socialfancysliderHead_position='top: 50%; margin-top: -18px;';

}

if($options['TabPosition']=='Top')

{

	$socialfancysliderHead_position='top: 5px;';

}

if($options['TabPosition']=='Bottom')

{	

	$socialfancysliderHead_position='bottom: 5px;';

}

if($options['TabPosition']=='Fixed')

{	

	$socialfancysliderHead_position='top: '.$options['TabPositionPx'].'px;';

}

?>

<div class="socialfancysliderCenterOuter socialfancysliderCenterOuterFb <?php echo ($options['VPosition']=='Fixed' ? 'socialfancysliderFixed': '')?> socialfancyslider<?php echo $options['Position']?>" style="margin-top:<?php echo $socialfancyslider_options['Box_TabPositionPx'];?>px; <?php echo ($options['Position']=='Left' ? 'left: -'.($options['Width']+$options['Border']).'px;' : 'right: -'.($options['Width']+$options['Border']).'px;')?><?php echo ($options['ZIndex'] ? 'z-index: '.$options['ZIndex'].';': '')?>">



	<div class="socialfancysliderCenterInner">

		<div class="socialfancysliderWrap socialfancysliderTheme0 socialfancysliderTab<?php echo $options['TabDesign']?>">

			<div class="socialfancysliderForm" style="background: <?php echo $options['BorderColor']?>; height: <?php echo $options['Height']?>px; width: <?php echo $options['Width']?>px; padding: <?php echo ($options['Position']=='Left' ? $options['Border'].'px '.$options['Border'].'px '.$options['Border'].'px 0' : $options['Border'].'px 0 '.$options['Border'].'px '.$options['Border'].'px')?>;">

				<h2 class="socialfancysliderHead" style="<?php echo $socialfancysliderHead_position; ?> <?php echo ($options['Position']=='Left' ? 'left: '.($options['Width']+$options['Border']).'px;' : 'right: '.($options['Width']+$options['Border']).'px;')?>">Facebook</h2>

				<div id="socialfancysliderInnerFb" class="socialfancysliderInner" style="height: <?php echo $options['Height']?>px; background-color: <?php echo $options['BackgroundColor']?>;">

					

			
					
				

					  <object type="text/html" style="width: <?php echo $options['Width'];?>px; height:<?php echo $options['Height']?>px;" data="http://www.facebook.com/plugins/likebox.php?locale=<?php echo $options['Locale']?>&href=<?php echo  $options['FacebookPageURL'] ?>&<?php if($options['ShowFaces1'] == 1){ ?>height=<?php echo $options['Height'];?>&<?php } else { ?>height=<?php echo $options['Height']?>& <?php }?>stream=<?php echo $options['ShowStream'];?>&header=<?php echo $options['ShowHeader'];?>&show_faces=<?php echo $options['ShowFaces'];?>" ></object>
					  
				</div>

			</div>

		</div>

	</div>

</div>