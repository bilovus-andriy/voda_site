<?php 

/**

* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya

 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html

 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant

 * to the GNU General Public License, and as distributed it includes or

 * is derivative of works licensed under the GNU General Public License or

 * other free or open source software licenses.

**/



// no direct access



if($facebook_direction == 1) { $facebook_c_direction = 'Left'; } else { $facebook_c_direction = 'Right'; }

if($facebook_header == 1) { $facebook_header = 'true'; } else { $facebook_header = 'false'; }
if($facebook_faces == 1) { $facebook_c_faces = 'true'; } else { $facebook_c_faces = 'false'; }
if($facebook_stream == 1) { $facebook_c_stream = 'true'; } else { $facebook_c_stream = 'false'; }

if($twitter_direction == 1) { $twitter_c_direction = 'Left'; } else { $twitter_c_direction = 'Right'; }

if($gplus_direction == 1) { $gplus_c_direction = 'Left'; } else { $gplus_c_direction = 'Right'; }

if($ytube_direction == 1) { $ytube_c_direction = 'Left'; } else { $ytube_c_direction = 'Right'; }

if($linkedin_direction == 1) { $linkedin_c_direction = 'Left'; } else { $linkedin_c_direction = 'Right'; }

if($linkedin_s_enable == 1) { $linkedin_s_enable = '1'; } else { $linkedin_s_enable = '0'; }

if($pinterest_direction == 1) { $pinterest_c_direction = 'Left'; } else { $pinterest_c_direction = 'Right'; }

if($pinterest_s_enable == 1) { $pinterest_s_enable = '1'; } else { $pinterest_s_enable = '0'; }



if($rssfeed_direction == 1) { $rssfeed_c_direction = 'Left'; } else { $rssfeed_c_direction = 'Right'; }

if($rssfeed_s_enable == 1) { $rssfeed_s_enable = '1'; } else { $rssfeed_s_enable = '0'; }



$socialfancyslider_options['Enable'] = $facebook_s_enable;

$socialfancyslider_options['FacebookPageURL'] = $facebook_page_url;

$socialfancyslider_options['Width'] = $facebook_slide_width;

$socialfancyslider_options['Height'] = $facebook_slide_height;

$socialfancyslider_options['ShowFaces'] = $facebook_c_faces;

$socialfancyslider_options['ShowFaces1'] = $facebook_faces;

$socialfancyslider_options['ShowStream'] = $facebook_c_stream;

$socialfancyslider_options['ForceWall'] = '';

$socialfancyslider_options['ShowHeader'] = $facebook_c_header;

$socialfancyslider_options['Position'] = $facebook_c_direction;

$socialfancyslider_options['TabPosition'] = 'Fixed';

$socialfancyslider_options['TabPositionPx'] = $facebook_top_margin;

$socialfancyslider_options['Box_TabPositionPx'] = $facebook_box_top_margin;

$socialfancyslider_options['TabDesign'] = $theme_style;

$socialfancyslider_options['Border'] = $facebook_border_size;

$socialfancyslider_options['BorderColor'] = $facebook_border_color;

$socialfancyslider_options['BackgroundColor'] = $facebook_background_color;

$socialfancyslider_options['Locale'] = $facebook_lang;

$socialfancyslider_options['ColorScheme'] = 'light';

$socialfancyslider_options['VPosition'] = 'Fixed';

$socialfancyslider_options['VPositionPx'] = '150px';

$socialfancyslider_options['ZIndex'] = '1000';


$socialfancyslider_options['TW_Enable'] = $twitter_s_enable;

$socialfancyslider_options['TW_Username'] = $twitter_username;

$socialfancyslider_options['TW_Width'] = $twitter_slide_width;

$socialfancyslider_options['TW_Height'] = $twitter_slide_height;

$socialfancyslider_options['TW_ShowFollowButton'] = '1';

$socialfancyslider_options['TW_Position'] = $twitter_c_direction;

$socialfancyslider_options['TW_TabPosition'] = 'Fixed';

$socialfancyslider_options['TW_TabPositionPx'] = $twitter_top_margin;

$socialfancyslider_options['TW_Box_TabPositionPx'] = $twitter_box_top_margin;

$socialfancyslider_options['TW_TabDesign'] = $theme_style;

$socialfancyslider_options['TW_Border'] = $twitter_border_size;

$socialfancyslider_options['TW_BorderColor'] = $twitter_border_color;

$socialfancyslider_options['TW_ShellBackground'] = '#33ccff';

$socialfancyslider_options['TW_ShellText'] = '#ffffff';

$socialfancyslider_options['TW_TweetBackground'] = $twitter_background_color;

$socialfancyslider_options['TW_TweetText'] = $twitter_txt_color;

$socialfancyslider_options['TW_Links'] = $twitter_link_color;

$socialfancyslider_options['TW_VPosition'] = 'Fixed';

$socialfancyslider_options['TW_VPositionPx'] = '160px';

$socialfancyslider_options['TW_ZIndex'] = '1000';

$socialfancyslider_options['TW_live'] = '';

$socialfancyslider_options['TW_behavior'] = 'all';

$socialfancyslider_options['TW_loop'] = '';

$socialfancyslider_options['TW_interval'] = '30';

$socialfancyslider_options['TW_rpp'] = '7';

$socialfancyslider_options['TW_Language'] = $twitter_lang;



$socialfancyslider_options['GP_Enable'] = $gplus_s_enable;

$socialfancyslider_options['GP_PageID'] = $gplus_id;

$socialfancyslider_options['GP_ShowFeed'] = '0';

$socialfancyslider_options['GP_Width'] = $gplus_slide_width;

$socialfancyslider_options['GP_Height'] = $gplus_slide_height;

$socialfancyslider_options['GP_Position'] = $gplus_c_direction;

$socialfancyslider_options['GP_TabPosition'] = 'Fixed';

$socialfancyslider_options['GP_TabPositionPx'] = $gplus_top_margin;

$socialfancyslider_options['GP_Box_TabPositionPx'] = $gplus_box_top_margin;

$socialfancyslider_options['GP_TabDesign'] = $theme_style;

$socialfancyslider_options['GP_Border'] = $gplus_border_size;

$socialfancyslider_options['GP_BorderColor'] = $gplus_border_color;

$socialfancyslider_options['GP_BackgroundColor'] = $gplus_background_color;

$socialfancyslider_options['GP_VPosition'] = 'Fixed';

$socialfancyslider_options['GP_VPositionPx'] = '170px';

$socialfancyslider_options['GP_ZIndex'] = '1000';

$socialfancyslider_options['GP_Language'] = $gplus_lang;



$socialfancyslider_options['YT_Enable'] = $ytube_s_enable;

$socialfancyslider_options['YT_Channel'] = $ytube_id;

$socialfancyslider_options['YT_Position'] = $ytube_c_direction;

$socialfancyslider_options['YT_TabPosition'] = 'Fixed';

$socialfancyslider_options['YT_TabPositionPx'] = $ytube_top_margin;

$socialfancyslider_options['YT_Box_TabPositionPx'] = $ytube_box_top_margin;

$socialfancyslider_options['YT_TabDesign'] = $theme_style;

$socialfancyslider_options['YT_Width'] = $ytube_slide_width;

$socialfancyslider_options['YT_Height'] = $ytube_slide_height;

$socialfancyslider_options['YT_Border'] = $ytube_border_size;

$socialfancyslider_options['YT_BorderColor'] = $ytube_border_color;

$socialfancyslider_options['YT_BackgroundColor'] = $ytube_background_color;

$socialfancyslider_options['YT_VPosition'] = 'Fixed';

$socialfancyslider_options['YT_VPositionPx'] = '180px';

$socialfancyslider_options['YT_ZIndex'] = '1000';



$socialfancyslider_options['LI_Enable'] = $linkedin_s_enable;

$socialfancyslider_options['LI_ShowPublicProfile'] = $linkedin_public_profile_enable;

$socialfancyslider_options['LI_ShowCompanyProfile'] = $linkedin_company_profile_enable;

$socialfancyslider_options['LI_PublicProfile'] = $linkedin_public_profile;

$socialfancyslider_options['LI_CompanyID'] = $linkedin_company_profile;

$socialfancyslider_options['LI_Order'] = '1';

$socialfancyslider_options['LI_Position'] = $linkedin_c_direction;

$socialfancyslider_options['LI_TabPosition'] = 'Fixed';

$socialfancyslider_options['LI_TabPositionPx'] = $linkedin_top_margin;

$socialfancyslider_options['LI_Box_TabPositionPx'] = $linkedin_box_top_margin;

$socialfancyslider_options['LI_TabDesign'] = $theme_style;

$socialfancyslider_options['LI_Width'] = $linkedin_slide_width;

$socialfancyslider_options['LI_Height'] = $linkedin_slide_height;

$socialfancyslider_options['LI_Border'] = $linkedin_border_size;

$socialfancyslider_options['LI_BorderColor'] = $linkedin_border_color;

$socialfancyslider_options['LI_BackgroundColor'] = $linkedin_background_color;

$socialfancyslider_options['LI_VPosition'] = 'Fixed';

$socialfancyslider_options['LI_VPositionPx'] = '200px';

$socialfancyslider_options['LI_ZIndex'] = '1000';





$socialfancyslider_options['PI_Enable'] = $pinterest_s_enable;

$socialfancyslider_options['PI_PageID'] = $pinterest_id;

$socialfancyslider_options['PI_TabPosition'] = 'Fixed';

$socialfancyslider_options['PI_Position'] = $pinterest_c_direction;

$socialfancyslider_options['PI_TabPositionPx'] = $pinterest_top_margin;

$socialfancyslider_options['PI_Box_TabPositionPx'] = $pinterest_box_top_margin;

$socialfancyslider_options['PI_TabDesign'] = $theme_style;

$socialfancyslider_options['PI_Width'] = $pinterest_slide_width;

$socialfancyslider_options['PI_Height'] = $pinterest_slide_height;

$socialfancyslider_options['PI_Border'] = $pinterest_border_size;

$socialfancyslider_options['PI_BorderColor'] = $pinterest_border_color;

$socialfancyslider_options['PI_BackgroundColor'] = $pinterest_background_color;

$socialfancyslider_options['PI_VPosition'] = 'Fixed';

$socialfancyslider_options['PI_VPositionPx'] = '200px';

$socialfancyslider_options['PI_ZIndex'] = '1000';

$socialfancyslider_options['PI_num_of_pins'] = $num_of_pins;





$socialfancyslider_options['RS_Enable'] = $rssfeed_s_enable;

$socialfancyslider_options['RS_url'] = $rssfeed_url;

$socialfancyslider_options['RS_TabPosition'] = 'Fixed';

$socialfancyslider_options['RS_Position'] = $rssfeed_c_direction;

$socialfancyslider_options['RS_TabPositionPx'] = $rssfeed_top_margin;

$socialfancyslider_options['RS_Box_TabPositionPx'] = $rssfeed_box_top_margin;

$socialfancyslider_options['RS_TabDesign'] = $theme_style;

$socialfancyslider_options['RS_Width'] = $rssfeed_slide_width;

$socialfancyslider_options['RS_Height'] = $rssfeed_slide_height;

$socialfancyslider_options['RS_Border'] = $rssfeed_border_size;

$socialfancyslider_options['RS_BorderColor'] = $rssfeed_border_color;

$socialfancyslider_options['RS_BackgroundColor'] = $rssfeed_background_color;

$socialfancyslider_options['RS_VPosition'] = 'Fixed';

$socialfancyslider_options['RS_VPositionPx'] = '200px';

$socialfancyslider_options['RS_ZIndex'] = '1000';



$socialfancyslider_options['RS_font_family'] = $font_family;

$socialfancyslider_options['RS_feed_bg_color'] = $feed_bg_color;

$socialfancyslider_options['RS_bg_shadow_color'] = $bg_shadow_color;

$socialfancyslider_options['RS_feed_border_color'] = $feed_border_color;

$socialfancyslider_options['RS_feed_title_font_size'] = $feed_title_font_size;

$socialfancyslider_options['RS_feed_title_font_color'] = $feed_title_font_color;



$socialfancyslider_options['RS_feed_date_font_size'] = $feed_date_font_size;

$socialfancyslider_options['RS_feed_desc_font_size'] = $feed_desc_font_size;

$socialfancyslider_options['RS_feed_desc_font_color'] = $feed_desc_font_color;






$socialfancyslider_options['DisableByGetParamN'] = '';

$socialfancyslider_options['DisableByGetParamV'] = '';



$socialfancyslider_options['MySQL_Host'] = 'localhost';

$socialfancyslider_options['MySQL_Database'] = '';

$socialfancyslider_options['MySQL_Username'] = '';

$socialfancyslider_options['MySQL_Password'] = '';

$socialfancyslider_options['MySQL_Prefix'] = '';

?>