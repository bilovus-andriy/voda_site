<?php 
/** 
* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
**/


global $wpdb;
if($options['RS_TabPosition']=='Middle' && in_array($options['RS_TabDesign'],array(3,6)))
{
	$socialfancysliderHead_RS_position='top: 50%; margin-top: -30px;';
}
if($options['RS_TabPosition']=='Middle' && in_array($options['RS_TabDesign'],array(1,2,4,5)))
{
	$socialfancysliderHead_RS_position='top: 50%; margin-top: -78px;';
}
if($options['RS_TabPosition']=='Middle' && in_array($options['RS_TabDesign'],array(7,8)))
{
	$socialfancysliderHead_RS_position='top: 50%; margin-top: -45px;';
}
if($options['RS_TabPosition']=='Middle' && in_array($options['RS_TabDesign'],array(9,10)))
{
	$socialfancysliderHead_RS_position='top: 50%; margin-top: -18px;';
}
if($options['RS_TabPosition']=='Middle' && in_array($options['RS_TabDesign'],array(11,13)))
{
	$socialfancysliderHead_RS_position='top: 50%; margin-top: -54px;';
}
if($options['RS_TabPosition']=='Middle' && in_array($options['RS_TabDesign'],array(12,14)))
{
	$socialfancysliderHead_RS_position='top: 50%; margin-top: -39px;';
}
if($options['RS_TabPosition']=='Top')
{
	$socialfancysliderHead_RS_position='top: 5px;';
}
if($options['RS_TabPosition']=='Bottom')
{	
	$socialfancysliderHead_RS_position='bottom: 5px;';
}
if($options['RS_TabPosition']=='Fixed')
{	
	$socialfancysliderHead_RS_position='top: '.$options['RS_TabPositionPx'].'px;';
}



?>




<style>
.rssFeed {
overflow:scroll;
height:<?php echo $options['RS_Height']-75?>px;
overflow-x:hidden;
}
.rssRow h4 {
	display: none;
}

.rssBody li { 
border-top:1px solid #666666;
padding:10px;
list-style:none;
}
 .feedget-container{background-color:#eee !important;font-family:"<?php echo $socialfancyslider_options['RS_font_family'];?>" !important;text-align:center;line-height:14px; font-size:18px !important;}
 
 .vertical .feedget-element {  
	  margin-bottom: background-color: <?php echo $socialfancyslider_options['RS_feed_bg_color'];?>; border: 1px solid <?php echo $socialfancyslider_options['RS_feed_border_color'];?>;
	  box-shadow: 0 -10px 25px <?php echo $socialfancyslider_options['RS_bg_shadow_color'];?> inset;}
	   .feedget-element-title  a {font-size:<?php echo $socialfancyslider_options['RS_feed_title_font_size'];?>; font-weight:700; color:<?php echo $socialfancyslider_options['RS_feed_title_font_color'];?>!important; display:block;
	  text-decoration:none; }
	  .feedget-element-date { font-size: <?php echo $socialfancyslider_options['RS_feed_date_font_size'];?>; color:#666666; display:block; margin-bottom:5px; }
	   .feedget-element-text{font-size:<?php echo $socialfancyslider_options['RS_feed_desc_font_size'];?>!important; color:<?php echo $socialfancyslider_options['RS_feed_desc_font_color'];?>; display:block; text-align:justify !important; }
.ui-widget-header { background:#6d6d6d !important; } 
</style>

<div class="socialfancysliderCenterOuter socialfancysliderCenterOuterRs <?php echo ($options['RS_VPosition']=='Fixed' ? 'socialfancysliderFixed': '')?> socialfancyslider<?php echo $options['RS_Position']?>" style="margin-top:<?php echo $options['PI_Box_TabPositionPx'];?>px; <?php echo ($options['RS_Position']=='Left' ? 'left: -'.($options['RS_Width']+$options['RS_Border']).'px;' : 'right: -'.($options['RS_Width']+$options['RS_Border']).'px;')?><?php echo ($options['RS_ZIndex'] ? 'z-index: '.$options['RS_ZIndex'].';': '')?>">

	<div class="socialfancysliderCenterInner">
		<div class="socialfancysliderWrap socialfancysliderTheme0 socialfancysliderTab<?php echo $options['RS_TabDesign']?>">
			<div class="socialfancysliderForm" style="background: <?php echo $options['RS_BorderColor']?>; height: <?php echo $options['RS_Height']?>px; width: <?php echo $options['RS_Width']?>px; padding: <?php echo ($options['RS_Position']=='Left' ? $options['RS_Border'].'px '.$options['RS_Border'].'px '.$options['RS_Border'].'px 0' : $options['RS_Border'].'px 0 '.$options['RS_Border'].'px '.$options['RS_Border'].'px')?>;">

<h2 class="socialfancysliderHead" style="<?php echo $socialfancysliderHead_RS_position; ?> <?php echo ($options['RS_Position']=='Left' ? 'left: '.($options['RS_Width']+$options['RS_Border']).'px;' : 'right: '.($options['RS_Width']+$options['RS_Border']).'px;')?>">Facebook</h2>

<div class="socialfancysliderInner" style="overflow: hidden; background: <?php echo $options['RS_BackgroundColor'];?>; height: <?php echo $options['RS_Height'];?>px;">				



 <link type="text/css" rel="stylesheet" href="<?php echo JURI::root(); ?>modules/mod_socialfancysliders/css/rssfeed.css" />

    <!-- css for jquery ui theme -->
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/black-tie/jquery-ui.css" rel="stylesheet" type="text/css"> 
	
  <!-- js for jquery and jquery ui-->
  
  

  
  <script type="text/javascript" src="<?php echo JURI::root(); ?>modules/mod_socialfancysliders/js/jquery-1.8.2.min.js"></script>		
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>

    <!-- js for feedget -->
    <script src="<?php echo JURI::root(); ?>modules/mod_socialfancysliders/js/rssfeed.js"></script>

  <script>
var $jx = jQuery.noConflict();
		$jx(function() {
			$jx('.yourDiv').feedget(
				{ 
					feed: '<?php echo $socialfancyslider_options['RS_url'];?>',
					loadingImg: 'loading.gif',
					entries:<?php echo $no_of_feed;?>,
					images:<?php echo $images_enable;?>,
					facebook:<?php echo $facebook_enable;?>,
					twitter:<?php echo $twitter_enable;?>,
				});
		});
	</script>



<div class="yourDiv" style="width:<?php echo $options['RS_Width'];?>px; height:<?php echo $options['RS_Height'];?>px; float:left;"></div>


</div></div></div></div></div>


