<?php
/**
* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
**/

// no direct access
global $wpdb;
if($options['LI_TabPosition']=='Middle' && in_array($options['LI_TabDesign'],array(3,6)))
{
	$socialfancysliderHead_LI_position='top: 50%; margin-top: -30px;';
}
if($options['LI_TabPosition']=='Middle' && in_array($options['LI_TabDesign'],array(1,2,4,5)))
{
	$socialfancysliderHead_LI_position='top: 50%; margin-top: -78px;';
}
if($options['LI_TabPosition']=='Middle' && in_array($options['LI_TabDesign'],array(7,8)))
{
	$socialfancysliderHead_LI_position='top: 50%; margin-top: -45px;';
}
if($options['LI_TabPosition']=='Middle' && in_array($options['LI_TabDesign'],array(9,10)))
{
	$socialfancysliderHead_LI_position='top: 50%; margin-top: -18px;';
}
if($options['LI_TabPosition']=='Middle' && in_array($options['LI_TabDesign'],array(11,13)))
{
	$socialfancysliderHead_LI_position='top: 50%; margin-top: -54px;';
}
if($options['LI_TabPosition']=='Middle' && in_array($options['LI_TabDesign'],array(12,14)))
{
	$socialfancysliderHead_LI_position='top: 50%; margin-top: -39px;';
}
if($options['LI_TabPosition']=='Top')
{
	$socialfancysliderHead_LI_position='top: 5px;';
}
if($options['LI_TabPosition']=='Bottom')
{	
	$socialfancysliderHead_LI_position='bottom: 5px;';
}
if($options['LI_TabPosition']=='Fixed')
{	
	$socialfancysliderHead_LI_position='top: '.$options['LI_TabPositionPx'].'px;';
}
?>
<div class="socialfancysliderCenterOuter socialfancysliderCenterOuterLi <?php echo ($options['LI_VPosition'] == 'Fixed' ? 'socialfancysliderFixed' : '') ?> socialfancyslider<?php echo $options['LI_Position'] ?>" style="margin-top:<?php echo $options['LI_Box_TabPositionPx'];?>px; <?php echo ($options['LI_Position'] == 'Left' ? 'left: -' . ($options['LI_Width'] + $options['LI_Border']) . 'px;' : 'right: -' . ($options['LI_Width'] + $options['LI_Border']) . 'px;') ?><?php echo ($options['LI_ZIndex'] ? 'z-index: ' . $options['LI_ZIndex'] . ';' : '') ?>;">
	<div class="socialfancysliderCenterInner">
		<div class="socialfancysliderWrap socialfancysliderTheme0 socialfancysliderTab<?php echo $options['LI_TabDesign'] ?>">
			<div class="socialfancysliderForm" style="background: <?php echo $options['LI_BorderColor'] ?>; height: <?php echo $options['LI_Height'] ?>px; width: <?php echo $options['LI_Width'] ?>px; padding: <?php echo ($options['LI_Position'] == 'Left' ? $options['LI_Border'] . 'px ' . $options['LI_Border'] . 'px ' . $options['LI_Border'] . 'px 0' : $options['LI_Border'] . 'px 0 ' . $options['LI_Border'] . 'px ' . $options['LI_Border'] . 'px') ?>;">
				<h2 class="socialfancysliderHead" style="<?php echo $socialfancysliderHead_LI_position; ?> <?php echo ($options['LI_Position'] == 'Left' ? 'left: ' . ($options['LI_Width'] + $options['LI_Border']) . 'px;' : 'right: ' . ($options['LI_Width'] + $options['LI_Border']) . 'px;') ?>">LinkedId</h2>
				<div class="socialfancysliderInner" style="overflow: hidden; background: <?php echo $options['LI_BackgroundColor'] ?>; height: <?php echo $options['LI_Height'] ?>px;">				
						<script src="http://platform.linkedin.com/in.js" type="text/javascript"></script>
						<?php if($options['LI_ShowCompanyProfile']==1 && $options['LI_Order']==2){
						?>
						<script type="IN/CompanyProfile" data-related="false" data-width="<?php echo $options['LI_Width'] ?>" data-id="<?php echo $options['LI_CompanyID'] ?>" data-format="inline" ></script>
						<?php 
						}
						if($options['LI_ShowPublicProfile']==1){
						?>
						<script type="IN/MemberProfile" data-related="false" data-width="<?php echo $options['LI_Width'] ?>" data-id="<?php echo $options['LI_PublicProfile'] ?>" data-format="inline"></script>
						<?php } ?>
						<?php if($options['LI_ShowCompanyProfile']==1 && $options['LI_Order']==1){
						?>
						<script type="IN/CompanyProfile" data-related="false" data-width="<?php echo $options['LI_Width'] ?>" data-id="<?php echo $options['LI_CompanyID'] ?>" data-format="inline" ></script>
						<?php 
						}
						?>
				</div>
			</div>
		</div>
	</div>
</div>
					