<?php
/**
* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
**/

// no direct access
global $wpdb;
if($options['GP_TabPosition']=='Middle' && in_array($options['GP_TabDesign'],array(3,6)))
{
	$socialfancysliderHead_GP_position='top: 50%; margin-top: -30px;';
}
if($options['GP_TabPosition']=='Middle' && in_array($options['GP_TabDesign'],array(1,2,4,5)))
{
	$socialfancysliderHead_GP_position='top: 50%; margin-top: -78px;';
}
if($options['GP_TabPosition']=='Middle' && in_array($options['GP_TabDesign'],array(7,8)))
{
	$socialfancysliderHead_GP_position='top: 50%; margin-top: -45px;';
}
if($options['GP_TabPosition']=='Middle' && in_array($options['GP_TabDesign'],array(9)))
{
	$socialfancysliderHead_GP_position='top: 50%; margin-top: -18px;';
}
if($options['GP_TabPosition']=='Top')
{
	$socialfancysliderHead_GP_position='top: 5px;';
}
if($options['GP_TabPosition']=='Bottom')
{	
	$socialfancysliderHead_GP_position='bottom: 5px;';
}
if($options['GP_TabPosition']=='Fixed')
{	
	$socialfancysliderHead_GP_position='top: '.$options['GP_TabPositionPx'].'px;';
}
?>
<div class="socialfancysliderCenterOuter socialfancysliderCenterOuterGp <?php echo ($options['GP_VPosition'] == 'Fixed' ? 'socialfancysliderFixed' : '') ?> socialfancyslider<?php echo $options['GP_Position'] ?>" style="margin-top:<?php echo $options['GP_Box_TabPositionPx'];?>px; <?php echo  ($options['GP_Position'] == 'Left' ? 'left: -' . ($options['GP_Width'] + $options['GP_Border']) . 'px;' : 'right: -' . ($options['GP_Width'] + $options['GP_Border']) . 'px;') ?><?php echo  ($options['GP_ZIndex'] ? 'z-index: ' . $options['GP_ZIndex'] . ';' : '') ?>">
	<div class="socialfancysliderCenterInner">
		<div class="socialfancysliderWrap socialfancysliderTheme0 socialfancysliderTab<?php echo $options['GP_TabDesign'] ?>">
			<div class="socialfancysliderForm" style="background: <?php echo $options['GP_BorderColor'] ?>; height: <?php echo  $options['GP_Height'] ?>px; width: <?php echo  $options['GP_Width'] ?>px; padding: <?php echo  ($options['GP_Position'] == 'Left' ? $options['GP_Border'] . 'px ' . $options['GP_Border'] . 'px ' . $options['GP_Border'] . 'px 0' : $options['GP_Border'] . 'px 0 ' . $options['GP_Border'] . 'px ' . $options['GP_Border'] . 'px') ?>;">
				<h2 class="socialfancysliderHead" style="<?php echo $socialfancysliderHead_GP_position; ?> <?php echo  ($options['GP_Position'] == 'Left' ? 'left: ' . ($options['GP_Width'] + $options['GP_Border']) . 'px;' : 'right: ' . ($options['GP_Width'] + $options['GP_Border']) . 'px;') ?>">Google Plus</h2>
				<div class="socialfancysliderInner" style="background: <?php echo $options['GP_BackgroundColor']?>; height: <?php echo  $options['GP_Height'] ?>px;">
					<div style="overflow: hidden; ">
						
                        
						
						<link rel="stylesheet" href="<?php echo JURI::root(); ?>modules/mod_socialfancysliders/css/gplus.css" type="text/css" charset="utf-8"/>
						
				
<script type="text/javascript" src="<?php echo JURI::root(); ?>modules/mod_socialfancysliders/js/jquery.googleplus-activity-1.0.min.js"></script>
	<script src="https://apis.google.com/js/plusone.js"></script>
		<link href="https://plus.google.com/<?php echo $options['GP_PageID'];?>" rel="publisher" /><script type="text/javascript">

            window.___gcfg = {lang: '<?php echo $params->get( 'locale' ); ?>'};

            (function() 

            {var po = document.createElement("script");

                po.type = "text/javascript"; po.async = true;po.src = "https://apis.google.com/js/plusone.js";

                var s = document.getElementsByTagName("script")[0];

                s.parentNode.insertBefore(po, s);

            })();</script>

						
							<script type="text/javascript">
						//<![CDATA[
jQuery.fn.googlePlusActivity.defaults.api_key = 'AIzaSyB1UZNnscjMDjjH-pi_XbnLRld2wAqi3Ek';
//]]> 
						</script>
						
						
						
						
						<g:plus href="https://plus.google.com/<?php echo $options['GP_PageID'];?>" width="300" height="190" theme="light"></g:plus>
					
						
						<div class="google-plus-activity" data-options="{ user: '<?php echo $options['GP_PageID'];?>' }" style="width:<?php echo $options['GP_Width']?>; 
						height:<?php echo  $options['GP_Height']-125;?>px; margin-top:-10px; font-size:13px;">
						
						</div>
                         
                        
					</div>
				
				</div>
			</div>
		</div>
	</div>
</div>