<?php
/**
* @Copyright Copyright (C) 2010- ... Hiral Padsumbiya
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * mod_socialfancyslider is Commercial software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
**/

// no direct access
if($options['TW_TabPosition']=='Middle' && in_array($options['TW_TabDesign'],array(3,6)))
{
	$socialfancysliderHead_TW_position='top: 50%; margin-top: -30px;';
}
if($options['TW_TabPosition']=='Middle' && in_array($options['TW_TabDesign'],array(1,2,4,5)))
{
	$socialfancysliderHead_TW_position='top: 50%; margin-top: -78px;';
}
if($options['TW_TabPosition']=='Middle' && in_array($options['TW_TabDesign'],array(7,8)))
{
	$socialfancysliderHead_TW_position='top: 50%; margin-top: -45px;';
}
if($options['TW_TabPosition']=='Middle' && in_array($options['TW_TabDesign'],array(9)))
{
	$socialfancysliderHead_TW_position='top: 50%; margin-top: -18px;';
}
if($options['TW_TabPosition']=='Top')
{
	$socialfancysliderHead_TW_position='top: 5px;';
}
if($options['TW_TabPosition']=='Bottom')
{	
	$socialfancysliderHead_TW_position='bottom: 5px;';
}
if($options['TW_TabPosition']=='Fixed')
{	
	$socialfancysliderHead_TW_position='top: '.$options['TW_TabPositionPx'].'px;';
}



?>

<style>
.twtr-widget h3 { display:none; font-weight:700;} 
.twtr-widget h4 .twtr-hd a{ font-weight:700;} 
</style>

<div class="socialfancysliderCenterOuter socialfancysliderCenterOuterTw <?php echo  ($options['TW_VPosition'] == 'Fixed' ? 'socialfancysliderFixed' : '') ?> socialfancyslider<?php echo  $options['TW_Position'] ?>" style="margin-top:<?php echo $options['TW_Box_TabPositionPx'];?>px; <?php echo  ($options['TW_Position'] == 'Left' ? 'left: -' . ($options['TW_Width'] + $options['TW_Border']) . 'px;' : 'right: -' . ($options['TW_Width'] + $options['TW_Border']) . 'px;') ?><?php echo  ($options['TW_ZIndex'] ? 'z-index: ' . $options['TW_ZIndex'] . ';' : '') ?>">
	<div class="socialfancysliderCenterInner">
		<div class="socialfancysliderWrap socialfancysliderTheme0 socialfancysliderTab<?php echo  $options['TW_TabDesign'] ?>">
			<div class="socialfancysliderForm" style="background: <?php echo  $options['TW_BorderColor'] ?>; height: <?php echo  $options['TW_Height'] ?>px; width: <?php echo  $options['TW_Width'] ?>px; padding: <?php echo  ($options['TW_Position'] == 'Left' ? $options['TW_Border'] . 'px ' . $options['TW_Border'] . 'px ' . $options['TW_Border'] . 'px 0' : $options['TW_Border'] . 'px 0 ' . $options['TW_Border'] . 'px ' . $options['TW_Border'] . 'px') ?>;">
				<h2 class="socialfancysliderHead" style="<?php echo $socialfancysliderHead_TW_position; ?> <?php echo  ($options['TW_Position'] == 'Left' ? 'left: ' . ($options['TW_Width'] + $options['TW_Border']) . 'px;' : 'right: ' . ($options['TW_Width'] + $options['TW_Border']) . 'px;') ?>">Twitter</h2>
				
				<div id="socialfancysliderInnerTw" class="socialfancysliderInner socialfancysliderInnerLoading" style="height: <?php echo $options['TW_Height']?>px;">
					<div id="socialfancysliderTww"></div>
					<script src="http://widgets.twimg.com/j/2/widget.js"></script>
					<script>
						jQuery(document).ready(function(){	
						new TWTR.Widget({
							id:	'socialfancysliderTww',
							version: 2,
							type: 'profile',
							rpp: <?php echo  $options['TW_rpp'] ?>,
							interval: <?php echo  $options['TW_interval'] * 1000 ?>,
							width: <?php echo  $options['TW_Width'] ?>,
							height: <?php echo  $options['TW_Height'] - 92 ?>,
							theme: {
								shell: {
									background: '<?php echo  $options['TW_ShellBackground'] ?>',
									color: '<?php echo  $options['TW_ShellText'] ?>'
								},
								tweets: {
									background: '<?php echo  $options['TW_TweetBackground'] ?>',
									
									color: '<?php echo  $options['TW_TweetText'] ?>',
									links: '<?php echo  $options['TW_Links'] ?>'
								}
							},
							features: {
								loop: <?php echo  ($options['TW_loop'] ? 'true' : 'false') ?>,
								live: <?php echo  ($options['TW_live'] ? 'true' : 'false') ?>,
								scrollbar: true,
								avatars: true,
								behavior: '<?php echo  $options['TW_behavior'] ?>'				  
							}
						}).render().setUser('<?php echo  $options['TW_Username'] ?>').start();
					   jQuery('#socialfancysliderInnerTw').removeClass('socialfancysliderInnerLoading');
					});
					</script>		
					<?php if($options['TW_ShowFollowButton']){?>
					<div class="socialfancysliderFollowTw">
						<a href="https://twitter.com/<?php echo  $options['TW_Username'] ?>" class="twitter-follow-button" data-width="130px" data-show-count="false" data-lang="<?php echo  $options['TW_Language'] ?>">Follow <?php echo  $options['TW_Username'] ?></a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>