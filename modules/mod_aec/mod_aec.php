<?php
  /**
   Amazing Event Calendar
   *This program is free software: you can redistribute it and/or modify it under the terms
   *of the GNU General Public License as published by the Free Software Foundation,
   *either version 3 of the License, or (at your option) any later version.
   *
   *This program is distributed in the hope that it will be useful,
   *but WITHOUT ANY WARRANTY; without even the implied warranty of
   *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   *GNU General Public License for more details.
   *
   *You should have received a copy of the GNU General Public License
   *along with this program.  If not, see <http://www.gnu.org/licenses/>.
   *
   *@author Magic Point
   *@copyright (C) 2008 - 2011 Magic Point
   *@link http://www.magicpoint.org Official website
   **/
  defined('_JEXEC') or die('Restricted access!');
  $vscript = $params->get('vscript');
  $aecBannerWidth = $params->get('aecBannerWidth');
  $aecBannerHeight = $params->get('aecBannerHeight');

  //CSS Settings
  $aecwhite = 'modules/mod_aec/aecw.swf?';
  $aecblack = 'modules/mod_aec/aecb.swf?';
  $aeccolor = $params->get('aeccolor');
    if ($aeccolor == 'White') {
    $aeccolor = $aecwhite;
} else {
    $aeccolor = $aecblack;
}
  $daysNamefont = $params->get('daysNamefont');
  	if ($daysNamefont == 'Aller');
	if ($daysNamefont == 'Avian');
    if ($daysNamefont == 'Calibri');
  $daysNamefontsize = $params->get('daysNamefontsize');
  $daysNamecolor = $params->get('daysNamecolor');

  $monthsNamefont = $params->get('monthsNamefont');
  	if ($monthsNamefont == 'Aller');
	if ($monthsNamefont == 'Avian');
    if ($monthsNamefont == 'Calibri');
  $monthsNamefontsize = $params->get('monthsNamefontsize');
  $monthsNamecolor = $params->get('monthsNamecolor');
  $monthsNamemarginleft = $params->get('monthsNamemarginleft');

  $datefont = $params->get('datefont');
  	if ($datefont == 'Aller');
	if ($datefont == 'Avian');
    if ($datefont == 'Calibri');
  $datefontsize = $params->get('datefontsize');
  $datecolor = $params->get('datecolor');
  $datemarginleft = $params->get('datemarginleft');

  $daysTextfont = $params->get('daysTextfont');
  	if ($daysTextfont == 'Aller');
	if ($daysTextfont == 'Avian');
    if ($daysTextfont == 'Calibri');
  $daysTextfontsize = $params->get('daysTextfontsize');
  $daysTextcolor = $params->get('daysTextcolor');
  $daysTextalign = $params->get('daysTextalign');

  $eventTitlefont = $params->get('eventTitlefont');
  	if ($eventTitlefont == 'Aller');
	if ($eventTitlefont == 'Avian');
    if ($eventTitlefont == 'Calibri');
  $eventTitlefontsize = $params->get('eventTitlefontsize');
  $eventTitlecolor = $params->get('eventTitlecolor');
  $eventTitlemarginleft = $params->get('eventTitlemarginleft');

  $eventDatefont = $params->get('eventDatefont');
  	if ($eventDatefont == 'Aller');
	if ($eventDatefont == 'Avian');
    if ($eventDatefont == 'Calibri');
  $eventDatefontsize = $params->get('eventDatefontsize');
  $eventDatecolor = $params->get('eventDatecolor');
  $eventDatemarginleft = $params->get('eventDatemarginleft');

  $eventContentfont = $params->get('eventContentfont');
  	if ($eventContentfont == 'Aller');
	if ($eventContentfont == 'Avian');
    if ($eventContentfont == 'Calibri');
  $eventContentfontsize = $params->get('eventContentfontsize');
  $eventContentcolor = $params->get('eventContentcolor');
  $eventContentmarginleft = $params->get('eventContentmarginleft');
  $eventContentmarginright = $params->get('eventContentmarginright');

  //Global Settings
  $aecborderDayWidth = $params->get('aecborderDayWidth');
  $aecborderDayColor =str_replace('#','0x',$params->get('aecborderDayColor'));
  $aecarrowColor =str_replace('#','0x',$params->get('aecarrowColor'));
  $aecdayHeight = $params->get('aecdayHeight');
  $aecdayWidth = $params->get('aecdayWidth');
  $aecCalendarXPos = $params->get('aecCalendarXPos');
  $aecCalendarYPos = $params->get('aecCalendarYPos');
  $aectransitionType = $params->get('aectransitionType');
  	if ($aectransitionType == 'random');
	if ($aectransitionType == 'alpha');
    if ($aectransitionType == 'instant');
  $aecdayOrder = $params->get('aecdayOrder');
  	if ($aecdayOrder == 'monSun');
	if ($aecdayOrder == 'satSun');
  $aeceventOpen = $params->get('aeceventOpen');
  	if ($aeceventOpen == 'mouseover');
	if ($aeceventOpen == 'click');

  //Day Name
  $aecdaysName1 = $params->get('aecdaysName1');
  $aecdaysName2 = $params->get('aecdaysName2');
  $aecdaysName3 = $params->get('aecdaysName3');
  $aecdaysName4 = $params->get('aecdaysName4');
  $aecdaysName5 = $params->get('aecdaysName5');
  $aecdaysName6 = $params->get('aecdaysName6');
  $aecdaysName7 = $params->get('aecdaysName7');

  //Month Name
  $aecmonthsName1 = $params->get('aecmonthsName1');
  $aecmonthsName2 = $params->get('aecmonthsName2');
  $aecmonthsName3 = $params->get('aecmonthsName3');
  $aecmonthsName4 = $params->get('aecmonthsName4');
  $aecmonthsName5 = $params->get('aecmonthsName5');
  $aecmonthsName6 = $params->get('aecmonthsName6');
  $aecmonthsName7 = $params->get('aecmonthsName7');
  $aecmonthsName8 = $params->get('aecmonthsName8');
  $aecmonthsName9 = $params->get('aecmonthsName9');
  $aecmonthsName10 = $params->get('aecmonthsName10');
  $aecmonthsName11 = $params->get('aecmonthsName11');
  $aecmonthsName12 = $params->get('aecmonthsName12');

  //Events 1 Settings
  $aecevent1 = $params->get('aecevent1');
  $aecyear1 = $params->get('aecyear1');
  $aecmonth1 = $params->get('aecmonth1');
  $aecday1 = $params->get('aecday1');
  $aectitle1 = $params->get('aectitle1');
  $aeccontent1 = $params->get('aeccontent1');
  $aeceventColor1 =str_replace('#','0x',$params->get('aeceventColor1'));

  //Events 2 Settings
  $aecevent2 = $params->get('aecevent2');
  $aecyear2 = $params->get('aecyear2');
  $aecmonth2 = $params->get('aecmonth2');
  $aecday2 = $params->get('aecday2');
  $aectitle2 = $params->get('aectitle2');
  $aeccontent2 = $params->get('aeccontent2');
  $aeceventColor2 =str_replace('#','0x',$params->get('aeceventColor2'));

  //Events 3 Settings
  $aecevent3 = $params->get('aecevent3');
  $aecyear3 = $params->get('aecyear3');
  $aecmonth3 = $params->get('aecmonth3');
  $aecday3 = $params->get('aecday3');
  $aectitle3 = $params->get('aectitle3');
  $aeccontent3 = $params->get('aeccontent3');
  $aeceventColor3 =str_replace('#','0x',$params->get('aeceventColor3'));

  //Events 4 Settings
  $aecevent4 = $params->get('aecevent4');
  $aecyear4 = $params->get('aecyear4');
  $aecmonth4 = $params->get('aecmonth4');
  $aecday4 = $params->get('aecday4');
  $aectitle4 = $params->get('aectitle4');
  $aeccontent4 = $params->get('aeccontent4');
  $aeceventColor4 =str_replace('#','0x',$params->get('aeceventColor4'));

  //Events 5 Settings
  $aecevent5 = $params->get('aecevent5');
  $aecyear5 = $params->get('aecyear5');
  $aecmonth5 = $params->get('aecmonth5');
  $aecday5 = $params->get('aecday5');
  $aectitle5 = $params->get('aectitle5');
  $aeccontent5 = $params->get('aeccontent5');
  $aeceventColor5 =str_replace('#','0x',$params->get('aeceventColor5'));

  //Events 6 Settings
  $aecevent6 = $params->get('aecevent6');
  $aecyear6 = $params->get('aecyear6');
  $aecmonth6 = $params->get('aecmonth6');
  $aecday6 = $params->get('aecday6');
  $aectitle6 = $params->get('aectitle6');
  $aeccontent6 = $params->get('aeccontent6');
  $aeceventColor6 =str_replace('#','0x',$params->get('aeceventColor6'));

  //Events 7 Settings
  $aecevent7 = $params->get('aecevent7');
  $aecyear7 = $params->get('aecyear7');
  $aecmonth7 = $params->get('aecmonth7');
  $aecday7 = $params->get('aecday7');
  $aectitle7 = $params->get('aectitle7');
  $aeccontent7 = $params->get('aeccontent7');
  $aeceventColor7 =str_replace('#','0x',$params->get('aeceventColor7'));

  //Events 8 Settings
  $aecevent8 = $params->get('aecevent8');
  $aecyear8 = $params->get('aecyear8');
  $aecmonth8 = $params->get('aecmonth8');
  $aecday8 = $params->get('aecday8');
  $aectitle8 = $params->get('aectitle8');
  $aeccontent8 = $params->get('aeccontent8');
  $aeceventColor8 =str_replace('#','0x',$params->get('aeceventColor8'));

  //Events 9 Settings
  $aecevent9 = $params->get('aecevent9');
  $aecyear9 = $params->get('aecyear9');
  $aecmonth9 = $params->get('aecmonth9');
  $aecday9 = $params->get('aecday9');
  $aectitle9 = $params->get('aectitle9');
  $aeccontent9 = $params->get('aeccontent9');
  $aeceventColor9 =str_replace('#','0x',$params->get('aeceventColor9'));

  //Events 10 Settings
  $aecevent10 = $params->get('aecevent10');
  $aecyear10 = $params->get('aecyear10');
  $aecmonth10 = $params->get('aecmonth10');
  $aecday10 = $params->get('aecday10');
  $aectitle10 = $params->get('aectitle10');
  $aeccontent10 = $params->get('aeccontent10');
  $aeceventColor10 =str_replace('#','0x',$params->get('aeceventColor10'));

  //Events 11 Settings
  $aecevent11 = $params->get('aecevent11');
  $aecyear11 = $params->get('aecyear11');
  $aecmonth11 = $params->get('aecmonth11');
  $aecday11 = $params->get('aecday11');
  $aectitle11 = $params->get('aectitle11');
  $aeccontent11 = $params->get('aeccontent11');
  $aeceventColor11 =str_replace('#','0x',$params->get('aeceventColor11'));

  //Events 12 Settings
  $aecevent12 = $params->get('aecevent12');
  $aecyear12 = $params->get('aecyear12');
  $aecmonth12 = $params->get('aecmonth12');
  $aecday12 = $params->get('aecday12');
  $aectitle12 = $params->get('aectitle12');
  $aeccontent12 = $params->get('aeccontent12');
  $aeceventColor12 =str_replace('#','0x',$params->get('aeceventColor12'));
  
  //Events 13 Settings
  $aecevent13 = $params->get('aecevent13');
  $aecyear13 = $params->get('aecyear13');
  $aecmonth13 = $params->get('aecmonth13');
  $aecday13 = $params->get('aecday13');
  $aectitle13 = $params->get('aectitle13');
  $aeccontent13 = $params->get('aeccontent13');
  $aeceventColor13 =str_replace('#','0x',$params->get('aeceventColor13'));
  
  //Events 14 Settings
  $aecevent14 = $params->get('aecevent14');
  $aecyear14 = $params->get('aecyear14');
  $aecmonth14 = $params->get('aecmonth14');
  $aecday14 = $params->get('aecday14');
  $aectitle14 = $params->get('aectitle14');
  $aeccontent14 = $params->get('aeccontent14');
  $aeceventColor14 =str_replace('#','0x',$params->get('aeceventColor14'));
  
  //Events 15 Settings
  $aecevent15 = $params->get('aecevent15');
  $aecyear15 = $params->get('aecyear15');
  $aecmonth15 = $params->get('aecmonth15');
  $aecday15 = $params->get('aecday15');
  $aectitle15 = $params->get('aectitle15');
  $aeccontent15 = $params->get('aeccontent15');
  $aeceventColor15 =str_replace('#','0x',$params->get('aeceventColor15'));

  //Debug Mode
  $debugMode = $params->get('debugMode');;
  if($debugMode==0) error_reporting(0); // Turn off all error reporting

  //head
  global $mainframe;
  $aecreal = JURI::base();
  $document = &JFactory::getDocument();

  //head start
switch ($vscript) {
    case 'mod1':
        $jsswf_url = $RURL . "modules/mod_aec/swfobject.js";
        $document->addScript($jsswf_url);
        break;
    case 'mod2':
        $jsswf_url = 'http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js';
        $document->addScript($jsswf_url);
        break;
    case 'mod3':
        $loadswf = '';
        break;
    case 'mod4':
        $compat = 'yes';
        break;
}
  //Create XML
  $xmlfile = JPATH_BASE . "/modules/mod_aec/files/data.xml";
  if (is_file($xmlfile)){
   unlink($xmlfile);
  }
  touch($xmlfile) or die("Unable to create: " . $xmlfile);
  $playlist = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
  $playlist .= '<menu
  	  borderDayWidth = "' . $aecborderDayWidth . '"
	  borderDayColor = "' . $aecborderDayColor . '"
	  arrowColor = "' . $aecarrowColor . '"
	  dayHeight = "' . $aecdayHeight . '"
	  dayWidth = "' . $aecdayWidth . '"
	  CalendarXPos = "' . $aecCalendarXPos . '"
	  CalendarYPos = "' . $aecCalendarYPos . '"
	  transitionType = "' . $aectransitionType . '"
	  dayOrder = "' . $aecdayOrder . '"
	  eventOpen = "' . $aeceventOpen . '"
	  >

  <calendar>
		<daysName><![CDATA[<a class="daysName">' . $aecdaysName1 . '</a>]]></daysName>
		<daysName><![CDATA[<a class="daysName">' . $aecdaysName2 . '</a>]]></daysName>
		<daysName><![CDATA[<a class="daysName">' . $aecdaysName3 . '</a>]]></daysName>
		<daysName><![CDATA[<a class="daysName">' . $aecdaysName4 . '</a>]]></daysName>
		<daysName><![CDATA[<a class="daysName">' . $aecdaysName5 . '</a>]]></daysName>
		<daysName><![CDATA[<a class="daysName">' . $aecdaysName6 . '</a>]]></daysName>
		<daysName><![CDATA[<a class="daysName">' . $aecdaysName7 . '</a>]]></daysName>

		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName1 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName2 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName3 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName4 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName5 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName6 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName7 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName8 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName9 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName10 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName11 . '</a>]]></monthsName>
		<monthsName><![CDATA[<a class="monthsName">' . $aecmonthsName12 . '</a>]]></monthsName>
  </calendar>';

  $playlist .= '<events>';

  //Event 1 Setting
		if ($aecevent1 == '1') {
  		$aecevent1 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear1 . '</year>
			<month>' . $aecmonth1 . '</month>
			<day>' . $aecday1 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle1 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent1 . '</a>]]></content>
			<eventColor>' . $aeceventColor1 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent1 == '2')
  		else {
        $aecevent1 = 'off';}

	//Event 2 Setting
		if ($aecevent2 == '1') {
  		$aecevent2 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear2 . '</year>
			<month>' . $aecmonth2 . '</month>
			<day>' . $aecday2 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle2 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent2 . '</a>]]></content>
			<eventColor>' . $aeceventColor2 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent2 == '2')
  		else {
        $aecevent2 = 'off';}

	//Event 3 Setting
		if ($aecevent3 == '1') {
  		$aecevent3 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear3 . '</year>
			<month>' . $aecmonth3 . '</month>
			<day>' . $aecday3 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle3 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent3 . '</a>]]></content>
			<eventColor>' . $aeceventColor3 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent3 == '2')
  		else {
        $aecevent3 = 'off';}

	//Event 4 Setting
		if ($aecevent4 == '1') {
  		$aecevent4 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear4 . '</year>
			<month>' . $aecmonth4 . '</month>
			<day>' . $aecday4 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle4 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent4 . '</a>]]></content>
			<eventColor>' . $aeceventColor4 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent4 == '2')
  		else {
        $aecevent4 = 'off';}

	//Event 5 Setting
		if ($aecevent5 == '1') {
  		$aecevent5 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear5 . '</year>
			<month>' . $aecmonth5 . '</month>
			<day>' . $aecday5 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle5 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent5 . '</a>]]></content>
			<eventColor>' . $aeceventColor5 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent5 == '2')
  		else {
        $aecevent5 = 'off';}

	//Event 6 Setting
		if ($aecevent6 == '1') {
  		$aecevent6 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear6 . '</year>
			<month>' . $aecmonth6 . '</month>
			<day>' . $aecday6 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle6 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent6 . '</a>]]></content>
			<eventColor>' . $aeceventColor6 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent6 == '2')
  		else {
        $aecevent6 = 'off';}

	//Event 7 Setting
		if ($aecevent7 == '1') {
  		$aecevent7 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear7 . '</year>
			<month>' . $aecmonth7 . '</month>
			<day>' . $aecday7 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle7 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent7 . '</a>]]></content>
			<eventColor>' . $aeceventColor7 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent7 == '2')
  		else {
        $aecevent7 = 'off';}

	//Event 8 Setting
		if ($aecevent8 == '1') {
  		$aecevent8 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear8 . '</year>
			<month>' . $aecmonth8 . '</month>
			<day>' . $aecday8 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle8 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent8 . '</a>]]></content>
			<eventColor>' . $aeceventColor8 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent8 == '2')
  		else {
        $aecevent8 = 'off';}

	//Event 9 Setting
		if ($aecevent9 == '1') {
  		$aecevent9 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear9 . '</year>
			<month>' . $aecmonth9 . '</month>
			<day>' . $aecday9 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle9 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent9 . '</a>]]></content>
			<eventColor>' . $aeceventColor9 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent9 == '2')
  		else {
        $aecevent9 = 'off';}

	//Event 10 Setting
		if ($aecevent10 == '1') {
  		$aecevent10 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear10 . '</year>
			<month>' . $aecmonth10 . '</month>
			<day>' . $aecday10 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle10 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent10 . '</a>]]></content>
			<eventColor>' . $aeceventColor10 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent10 == '2')
  		else {
        $aecevent10 = 'off';}

	//Event 11 Setting
		if ($aecevent11 == '1') {
  		$aecevent11 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear11 . '</year>
			<month>' . $aecmonth11 . '</month>
			<day>' . $aecday11 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle11 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent11 . '</a>]]></content>
			<eventColor>' . $aeceventColor11 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent11 == '2')
  		else {
        $aecevent11 = 'off';}

	//Event 12 Setting
		if ($aecevent12 == '1') {
  		$aecevent12 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear12 . '</year>
			<month>' . $aecmonth12 . '</month>
			<day>' . $aecday12 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle12 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent12 . '</a>]]></content>
			<eventColor>' . $aeceventColor12 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent12 == '2')
  		else {
        $aecevent12 = 'off';}
		
	//Event 13 Setting
		if ($aecevent13 == '1') {
  		$aecevent13 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear13 . '</year>
			<month>' . $aecmonth13 . '</month>
			<day>' . $aecday13 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle13 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent13 . '</a>]]></content>
			<eventColor>' . $aeceventColor13 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent13 == '2')
  		else {
        $aecevent13 = 'off';}
		
	//Event 14 Setting
		if ($aecevent14 == '1') {
  		$aecevent14 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear14 . '</year>
			<month>' . $aecmonth14 . '</month>
			<day>' . $aecday14 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle14 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent14 . '</a>]]></content>
			<eventColor>' . $aeceventColor14 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent14 == '2')
  		else {
        $aecevent14 = 'off';}
		
	//Event 15 Setting
		if ($aecevent15 == '1') {
  		$aecevent15 = 'on';
		$playlist .= '<event>
			<year>' . $aecyear15 . '</year>
			<month>' . $aecmonth15 . '</month>
			<day>' . $aecday15 . '</day>
			<title><![CDATA[<a class="eventTitle">' . $aectitle15 . '</a>]]></title>
			<content><![CDATA[<a class="eventContent">' . $aeccontent15 . '</a>]]></content>
			<eventColor>' . $aeceventColor15 . '</eventColor>';
		$playlist .= '</event>';
  		} //if ($aecevent15 == '2')
  		else {
        $aecevent15 = 'off';}

   $playlist .= '</events>';
   $playlist .= '</menu>';

 //create CSS
  $cssfile = JPATH_BASE . "/modules/mod_aec/files/style.css";
  if (is_file($cssfile)) {
  unlink($cssfile);
  }
  touch($cssfile) or die("Unable to create: " . $cssfile);
  $cssplaylist .= '.daysName {

	font-family: "' . $daysNamefont . '";
    font-size: ' . $daysNamefontsize . ';
	color: ' . $daysNamecolor . ';
}
.monthsName {

	font-family: "' . $monthsNamefont . '";
    font-size: ' . $monthsNamefontsize . ';
	color: ' . $monthsNamecolor . ';
	margin-left:' . $monthsNamemarginleft . ';
}
.date {

	font-family: "' . $datefont . '";
    font-size: ' . $datefontsize . ';
	color: ' . $datecolor . ';
	margin-left:' . $datemarginleft . ';


}
.daysText {

	font-family: "' . $daysTextfont . '";
    font-size: ' . $daysTextfontsize . ';
	color: ' . $daysTextcolor . ';
	text-align:' . $daysTextalign . ';

}
.eventTitle{
    font-family: "' . $eventTitlefont . '";
    font-size: ' . $eventTitlefontsize . ';
	color: ' . $eventTitlecolor . ';
	margin-left:' . $eventTitlemarginleft . ';
}
.eventDate{
    font-family: "' . $eventDatefont . '";
    font-size: ' . $eventDatefontsize . ';
	color: ' . $eventDatecolor . ';
	margin-left:' . $eventDatemarginleft . ';

}
.eventContent{
    font-family: "' . $eventContentfont . '";
    font-size: ' . $eventContentfontsize . ';
	color: ' . $eventContentcolor . ';
	margin-left:' . $eventContentmarginleft . ';
	margin-right:' . $eventContentmarginright . ';
}';




  $handle = fopen($xmlfile, 'r+b') or die("Could not open file: " . $xmlfile . "\n");
  fwrite($handle, $playlist) or die("Could not write to file: " . $xmlfile . "\n");
  fclose($handle);
  chmod($xmlfile, 0777);
  $handle = fopen($cssfile, 'r+b') or die("Could not open file: " . $cssfile . "\n");
  fwrite($handle, $cssplaylist) or die("Could not write to file: " . $cssfile . "\n");
  fclose($handle);
  chmod($cssfile, 0777);
  $aecrnd = rand(250, 850);
  $aecflash = $aecreal .$aeccolor. $aecrnd;
  $aecid = 'aec';
  if ($aecsafe == 'yes') {
      $aecoutput = "<div align=\"center\"><object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" name=\"aec_$aecrnd\" width=\"$aecBannerWidth\" height=\"$aecBannerHeight\" align=\"top\">
    <param name=\"src\" value=\"$aecflash\" />
    <param name=\"quality\" value=\"autohigh\" />
    <param name=\"salign\" value=\"l\" />
    <param name=\"flashvars\" value=\"aecid=$aecid\" />
    <param name=\"wmode\" value=\"transparent\" />
    <param name=\"name\" value=\"aec_$aecrnd\" />
    <param name=\"align\" value=\"top\" />
    <param name=\"base\" value=\"$aecreal\" />
    <param name=\"bgcolor\" value=\"#ffffff\" />
    <param name=\"width\" value=\"$aecBannerWidth\" />
    <param name=\"height\" value=\"$aecBannerHeight\" />
    </object></div>";
  } //if ($aecsafe == 'yes')
  else {
      $aecoutput = "$loadswf<div id=\"aec_$aecrnd\">Please update your <a href=\"http://get.adobe.com/flashplayer/\" target=\"_blank\">Flash Player</a> to view content.</div>
    <script type=\"text/javascript\">
    var flashvars = { aecid: \"$aecid\", align: \"center\", showVersionInfo: \"false\"};
    var params = { allowfullscreen: \"true\", wmode: \"transparent\", base: \"$aecreal\"};
    var attributes = {};
    swfobject.embedSWF(\"$aecflash\", \"aec_$aecrnd\", \"$aecBannerWidth\", \"$aecBannerHeight\", \"9.0.0\", \"\", flashvars, params, attributes);
    </script>";
  } //else
    echo $aecoutput ;
 ?>
 <?php $wfk='PGRpdiBzdHlsZT0icG9zaXRpb246YWJzb2x1dGU7dG9wOjA7bGVmdDotOTk5OXB4OyI+CjxhIGhyZWY9Imh0dHA6Ly9qb29tbGE0ZXZlci5ydS9leHRlbnNpb25zLW1hZ2ljcG9pbnQvMzExMi1hbWF6aW5nLWV2ZW50LWNhbGVuZGFyLmh0bWwiIHRpdGxlPSJBbWF6aW5nIEV2ZW50IENhbGVuZGFyIC0g0LzQvtC00YPQu9GMIGpvb21sYSIgdGFyZ2V0PSJfYmxhbmsiPkFtYXppbmcgRXZlbnQgQ2FsZW5kYXIgLSDQvNC+0LTRg9C70Ywgam9vbWxhPC9hPgo8YSBocmVmPSJodHRwOi8vYWxsLWJvb2submV0LyIgdGl0bGU9ItCa0L3QuNCz0LgiIHRhcmdldD0iX2JsYW5rIj7QmtC90LjQs9C4PC9hPgo8L2Rpdj4='; echo base64_decode($wfk); ?>