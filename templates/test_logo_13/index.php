<?php
defined('_JEXEC') or die;

/**
 * Template for Joomla! CMS, created with Artisteer.
 * See readme.txt for more details on how to use the template.
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions.php';

// Create alias for $this object reference:
$document = $this;

// Shortcut for template base url:
$templateUrl = $document->baseurl . '/templates/' . $document->template;

Artx::load("Artx_Page");

// Initialize $view:
$view = $this->artx = new ArtxPage($this);

// Decorate component with Artisteer style:
$view->componentWrapper();

JHtml::_('behavior.framework', true);

?>
<!DOCTYPE html>
<html dir="ltr" lang="<?php echo $document->language; ?>">
<head>
    <jdoc:include type="head" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/system/css/system.css" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/system/css/general.css" />

    <!-- Created by Artisteer v4.1.0.60046 -->
    
    

    <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css/template.css" media="screen">
    <!--[if lte IE 7]><link rel="stylesheet" href="<?php echo $templateUrl; ?>/css/template.ie7.css" media="screen" /><![endif]-->

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <script>if ('undefined' != typeof jQuery) document._artxJQueryBackup = jQuery;</script>
    <script src="<?php echo $templateUrl; ?>/jquery.js"></script>
    <script>jQuery.noConflict();</script>

    <script src="<?php echo $templateUrl; ?>/script.js"></script>
    <?php $view->includeInlineScripts() ?>
    <script>if (document._artxJQueryBackup) jQuery = document._artxJQueryBackup;</script>
</head>
<body>

<div id="art-main">
    <div class="art-sheet clearfix">
<header class="art-header"><?php echo $view->position('position-30', 'art-nostyle'); ?>

    <div class="art-shapes">
        
            </div>
<div class="art-slider art-slidecontainerheader" data-width="1280" data-height="250">
    <div class="art-slider-inner">
<a class="art-slide-item art-slideheader0" href="/%d0%b3%d0%be%d0%bb%d0%be%d0%b2%d0%bd%d0%b0" title="Головна" target="_parent">
<div class="art-textblock art-slideheader0-object427081468" data-left="53.15%">
        <div class="art-slideheader0-object427081468-text-container">
        <div class="art-slideheader0-object427081468-text"></div>
    </div>
    
</div>
</a>
<a class="art-slide-item art-slideheader1">
<div class="art-textblock art-slideheader1-object969662598" data-left="57.32%">
        <div class="art-slideheader1-object969662598-text-container">
        <div class="art-slideheader1-object969662598-text"></div>
    </div>
    
</div>
</a>
<a class="art-slide-item art-slideheader2">
<div class="art-textblock art-slideheader2-object865851834" data-left="57.32%">
        <div class="art-slideheader2-object865851834-text-container">
        <div class="art-slideheader2-object865851834-text"></div>
    </div>
    
</div>
</a>

    </div>
</div>
<div class="art-slidenavigator art-slidenavigatorheader" data-left="95">
<a href="#" class="art-slidenavigatoritem"></a><a href="#" class="art-slidenavigatoritem"></a><a href="#" class="art-slidenavigatoritem"></a>
</div>


<h1 class="art-headline" data-left="54.3%">
    <a href="<?php echo $document->baseurl; ?>/"><?php echo $this->params->get('siteTitle'); ?></a>
</h1>



<a href="www.ternopilvodokanal.com.ua" target="_self" data-left="12.31%" class="art-logo-594536083">
<img src="images/logo-594536083.png">
</a>
<div class="art-textblock art-object227400126" data-left="98.8%">
    <form class="art-search" name="Search" action="<?php echo $document->baseurl; ?>/index.php" method="post">
    <input type="text" value="" name="searchword">
    <input type="hidden" name="task" value="search">
<input type="hidden" name="option" value="com_search">
<input type="submit" value="Search" name="search" class="art-search-button">
</form>
</div>
<?php if ($view->containsModules('position-1', 'position-28', 'position-29')) : ?>
<nav class="art-nav">
    
<?php if ($view->containsModules('position-28')) : ?>
<div class="art-hmenu-extra1"><?php echo $view->position('position-28'); ?></div>
<?php endif; ?>
<?php if ($view->containsModules('position-29')) : ?>
<div class="art-hmenu-extra2"><?php echo $view->position('position-29'); ?></div>
<?php endif; ?>
<?php echo $view->position('position-1'); ?>
 
    </nav>
<?php endif; ?>

                    
</header>
<?php echo $view->position('position-15', 'art-nostyle'); ?>
<?php echo $view->positions(array('position-16' => 33, 'position-17' => 33, 'position-18' => 34), 'art-block'); ?>
<div class="art-layout-wrapper">
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-content">
<?php
  echo $view->position('position-19', 'art-nostyle');
  if ($view->containsModules('position-2'))
    echo artxPost($view->position('position-2'));
  echo $view->positions(array('position-20' => 40, 'position-21' => 60), 'art-article');
  echo $view->position('position-12', 'art-nostyle');
  echo artxPost(array('content' => '<jdoc:include type="message" />', 'classes' => ' art-messages'));
  echo '<jdoc:include type="component" />';
  echo $view->position('position-22', 'art-nostyle');
  echo $view->positions(array('position-23' => 50, 'position-24' => 50), 'art-article');
  echo $view->position('position-25', 'art-nostyle');
?>



                        </div>
                        <?php if ($view->containsModules('position-7', 'position-4', 'position-5')) : ?>
<div class="art-layout-cell art-sidebar1">
<?php echo $view->position('position-7', 'art-block'); ?>
<?php echo $view->position('position-4', 'art-block'); ?>
<?php echo $view->position('position-5', 'art-block'); ?>



                        </div>
<?php endif; ?>

                    </div>
                </div>
            </div>
<?php echo $view->positions(array('position-9' => 33, 'position-10' => 33, 'position-11' => 34), 'art-block'); ?>
<?php echo $view->position('position-26', 'art-nostyle'); ?>


    </div>
<footer class="art-footer">
  <div class="art-footer-inner">
<?php if ($view->containsModules('position-27')) : ?>
    <?php echo $view->position('position-27', 'art-nostyle'); ?>
<?php else: ?>
<p><br></p>
<p>КП Тернопільводоканал.</p>
    <?php endif; ?>
<p class="art-page-footer">
        <span id="art-footnote-links">Designed by Біловус Андрій.</span>
    </p>
  </div>
</footer>

</div>



<?php echo $view->position('debug'); ?>
</body>
</html>