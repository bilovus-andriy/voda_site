jQuery.gvalidation.errors = jQuery.extend(jQuery.gvalidation.errors, {
	required: "Поле обовязкове для заповнення.",
	alpha: "Дане поле може мати тільки букви.",
	alphanum: "Поле може мати тільки букви або цифри.",
	nodigit: "Поле може мати тільки цифри.",
	digit: "Будь-ласка введіть число.",
	digitmin: "Число не может быть менее %1",
	digitltd: "Число должно быть более %1 и менее %2",
	number: "Будь-ласка введіть число.",
	email: "Будь-оаска вкажіть правильний email: <br /><span>Наприклад  yourname@domain.ua</span>",
	phone: "Пожалуйста укажите правильный телефон.",
	url: "Будь-ласка вкажіть правильний адрес url: <br /><span>Наприклад http://www.domain.ua</span>",
	
	confirm: "Это поле не должно отличаться от поля %1",
	differs: "Число должно отличаться от %1",
	length_str: "Длина строки должна быть от %1 до %2 символов",
	length_fix: "The length is incorrect, it must be exactly %1 characters",
	lengthmax: "Заполните поле строкой, состоящей не более чем из %1 символов",
	lengthmin: "Заполните поле строкой, состоящей не менее чем из %1 символов",
	checkbox: "Пожалуйста поставьте галочку",
	group: 'Please make at least %1 selection(s).',
	custom: "Будь-ласка виберіть із уже запропонованих варіантів",
	select: "Будь-ласка виберіть із уже запропопнованих варіантів"
});